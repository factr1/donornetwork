<?php include("../common/header.php"); ?>

	<link rel="stylesheet" type="text/css" media="screen, print" href="slickmap.css" />

	<div class="sitemap">
		
		<h2>Preliminary Site Map &mdash; Version 1</h2>
<!-- 		<a href="sitemapv1.php">See version 1.0</a> -->


	
		<ul id="utilityNav">
		<p>Utility navigation</p>
			<li><a href="" title="">Home</a></li>
			<li><a href="" title="">Newsroom</a></li>
			<li><a href="" title="">Contact Us</a></li>
			<li><a href="" title="">Register as a donor</a></li>
			<li><a href="" title="">&nbsp;<i class="fa fa-search"></i>&nbsp;</a></li>
		</ul>

		<ul id="primaryNav" class="col8">
			<li id=""><a href="#">Home</a></li>
			<li id=""><a href="#">About Us</a>
					<ul>
						<li id=""><a href="#">Our Services</a>
								<ul>
								<li id=""><a href="#">Organ Recovery</a></li>
								<li id=""><a href="#">Tissue & Eye Recovery</a></li>
								<li id=""><a href="#">Immunogenetics Laboratory</a></li>
								<li id=""><a href="#">Public Education</a></li>
								<li id=""><a href="#">Donor Program Development</a></li>
								<li id=""><a href="#">Donor Family & Advocate Services</a></li>
								<li id=""><a href="#">Referral & Donor Services</a></li>
								<li id=""><a href="#">Regulatory Certificates</a></li>
								</ul>
						</li>

						<li id=""><a href="#">History</a></li>
						<li id=""><a href="#">Careers</a></li>
						<li id=""><a href="#">Newsletters</a></li>
						<li id=""><a href="#">	Contact</a></li>
						<li id=""><a href="#">Make a Contribution</a></li>

			</ul>
			</li>
			<li id=""><a href="#">Understanding Donation</a>
			<ul>
					<li id=""><a href="#">About Donation</a>
								<ul>
								<li id=""><a href="#">Organ Donation</a></li>
								<li id=""><a href="#">Tissue Donation</a></li>
								<li id=""><a href="#">Eye Donation</a></li>
								</ul>
					</li>

					<li id=""><a href="#">Perspectives on Donation</a>
							<ul>
								<li id=""><a href="#">Donation & Religion</a></li>
								<li id=""><a href="#">Multicultural Perspectives</a></li>
							</ul>
					</li>
					<li id=""><a href="#">Additional Resources</a></li>

			</ul>
			</li>
			<li id=""><a href="#">Donor Families</a>
						<ul>
								<li id=""><a href="#">Honoring Your Loved One</a></li>
								<li id=""><a href="#">Connecting with a Recipient</a>
										<ul>
												<li id=""><a href="#">Communication Guidelines</a></li>
												<li id=""><a href="#">Tips</a></li>
										</ul>
								</li>
								<li id=""><a href="#">Donor Quilts</a></li>
								<li id=""><a href="#">Online Resources</a></li>
						</ul>
			</li>
<!-- 			// End Donor Families -->
			
			<li id=""><a href="#">Transplant Recipients</a>
					<ul>
						<li id=""><a href="#">Sharing Your Story</a></li>
						<li id=""><a href="#">Writing to Your Donor Family</a>
								<ul>
								<li id=""><a href="#">Sample letters</a></li>
								<li id=""><a href="#">Recipient Reflections</a></li>
								<li id=""><a href="#">Communication Guidelines</a></li>
								</ul>
						</li>
						<li id=""><a href="#">Transplant Centers</a></li>
					</ul>
			</li>
<!-- 			// END Transplant Recipients -->


			<li id=""><a href="#">Professional Partners</a>
							<ul>
								<li id=""><a href="#">Hospital Professionals</a>
										<ul>
												<li id=""><a href="#">Referral Process</a></li>
												<li id=""><a href="#">Brain Death & Circulatory Death</a></li>
												<li id=""><a href="#">Families Understanding Brain Death</a></li>
												<li id=""><a href="#">Donation After Circulatory Death</a></li>
												<li id=""><a href="#">Donor Designation</a></li>
												<li id=""><a href="#">Family Conversations & Support</a></li>
												<li id=""><a href="#">Continuing Support</a></li>
												<li id=""><a href="#">Huddles</a></li>
												<li id=""><a href="#">Donor Management</a></li>
												<li id=""><a href="#">Eye & Tissue Recovery</a></li>
												<li id=""><a href="#">Regulatory Compliance</a></li>
												<li id=""><a href="#">Hospital Education</a></li>
												<li id=""><a href="#">Organ Donation Toolbox</a></li>
												<li id=""><a href="#">Health Care for Hope</a></li>
												<li id=""><a href="#">Your Donation Coordinator</a></li>
										</ul>
								</li>
								<!-- 			// END Professional Partners -->
								
								<li id=""><a href="#">Hospice Professionals</a></li>
								
								<li id=""><a href="#">Medical Examiners</a>
<!--
								Hidden for now per basecamp message 07.02.15
										<ul>
											<li id=""><a href="#">Medical Examiner Authorization Process</a></li>
											<li id=""><a href="#">Post-Recovery Paperwork</a></li>
											<li id=""><a href="#">Potential Donor Hold Process</a></li>
											<li id=""><a href="#">Donation Process</a></li>
											<li id=""><a href="#">Eye Recovery</a></li>
											<li id=""><a href="#">Tissue Recovery</a></li>
											<li id=""><a href="#">Required Information</a></li>
										</ul>
-->
								</li>
								
								<li id=""><a href="#">Funeral Directors</a></li>
								<li id=""><a href="#">Eye Surgeons</a></li>
								<li id=""><a href="#">Designated Requestor Certification</a></li>
								
							</ul>
			</li>
<!-- 			// END professional partners -->
			
			
			<li id=""><a href="#">Get Involved</a>
					<ul>
							<li id=""><a href="#">Volunteer</a></li>
							<li id=""><a href="#">Volunteer Forms</a></li>
							<li id=""><a href="#">Volunteer Newsletter</a></li>
							<li id=""><a href="#">Calendar</a></li>
							<li id=""><a href="#">Partnerships</a></li>
					</ul>
			</li>
<!-- 			// END Get Involved -->
			
			
			
			<li id=""><a href="#">News Room</a>
					<ul>
						<li id=""><a href="#">Media Kit</a></li>
						<li id=""><a href="#">Press Releases</a></li>
						<li id=""><a href="#">Blog</a></li>
						<li id=""><a href="#">Media Contact</a></li>
					</ul>
			</li>
		</ul>
		
		
		
		
		<ul id="utilityNav" class="footer">
		<p>Footer navigation</p>
			<li><a href="" title="">Home</a></li>
			<li><a href="" title="">Contact Us</a></li>
			<li><a href="" title="">Register as a donor</a></li>
			<li><a href="" title="">Privacy Policy</a></li>
			<li><a href="" title="">Sitemap</a></li>
			<li><a href="" title="">Terms of Use</a></li>
		</ul>
		
		
		

	</div>
	

	
</body>

</html>
