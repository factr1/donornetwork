<?php 
	
	include("/home/dnaz/public_html/password_protect.php"); 

	$date = strtotime("August 17, 2015 7:00 PM");
	$remaining = $date - time();
	$days_remaining = floor($remaining / 86400);
	$hours_remaining = floor(($remaining % 86400) / 3600);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>factor1</title>


<link type="text/css" rel="stylesheet" href="//cdn.jsdelivr.net/foundation/5.0.2/css/foundation.min.css">
<link type="text/css" rel="stylesheet" href="//cdn.jsdelivr.net/foundation/5.0.2/css/normalize.css" />
<link type="text/css" rel="stylesheet"  href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<link rel="stylesheet" href="http://72.52.245.2/~dnaz/hub/css/admin.css"> 

</head>

<body>
<header>
	<div class="row">
	
	<div class="medium-6 columns">
		<h1>DNAZ Project Dashboard</h1>
		<h3>Time to site launch: <?php echo $days_remaining?> days | 
		 <?php echo $hours_remaining?>  hours.
		 </h3>
	</div>
	
		<p class="medium-6 columns text-right" style="margin-top:35px;">
			<a href="http://factor1hosting.com/~dnaz/">Project hub home <i class="fa fa-home"></i></a> 
			<br> 
			<a class="logout" href="http://factor1hosting.com/~dnaz/?logout=1">Logout <i class="fa fa-sign-out"></i></a>
		</p>
	</div>
</header>