<?php include("../common/header.php"); 
	
	$filename = 'index.php';
	$timezone = 'America/Phoenix';
	date_default_timezone_set($timezone);
	
	if (file_exists($filename)) {
	    //echo "$filename was last modified: " . date ("F d Y H:i:s.", filemtime($filename));
	}


?>

	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>

		<div class="row">
			<h1 class="medium-6 columns">Development Timeline</h1>
			<p class="medium-4 columns" style="color:#ccc; margin-top:30px">	<?php echo "Last modified: " . date ("F d Y h:i a.", filemtime($filename));	?></p>
		</div>
	
	
	<section class="row">
		<ol class="timeline">
			<li class="tl-node completed">
				<h4 class="tl-stamp">Admin: June 30, 2015</h4>
				<h4>Contract and Invoice Delivered</h4>
				<div class="tl-content">
					<a href="#"><i class="fa fa-check-circle"></i>  Deliver Deposit Invoice</a>
					<a href="#"><i class="fa fa-check-circle"></i>  Pay Deposit invoice</a>
				</div>
			</li>
			
			<li class="tl-node completed">
				<h4 class="tl-stamp">Admin: July 2, 2015</h4>
				<h4>Site mapping and Project timeline completed</h4>
				<div class="tl-content">
						<p>Based on your sitemap, content flow and the proposal, we will have the timeline and sitemap finalized for your review. </p>
						<a href="#"><i class="fa fa-check-circle"></i>  Completed July 6 - MA </a>
				</div>
				<!--
				<a href="#">View meeting notes</a> 
				<a href="#">View client deliverable checklist</a> 
				-->
			</li>
			
			<li class="tl-node completed">
				<h4 class="tl-stamp">Strategy: July  6, 2015</h4>
				<h4>Wireframes</h4>
				<div class="tl-content">
						<p>Wireframes are the blueprints of the site design layouts. We use these to work out any issues, sort out the user flow, and navigation patterns. </p>
					<a href="#"><i class="fa fa-check-circle"></i>  Completed July 6 - MA </a>
				</div>
			</li>
			
			
			<li class="tl-node completed">
				<h4 class="tl-stamp">Design: July  10, 2015</h4>
				<h4>Initial Design Comps</h4>
				<div class="tl-content">
						<p>At this point we have spent a few days working on the design direction, colors, typography, and general aesthetics of your new site. We'd like to present these for discussion. These initial site designs are based on our conversations, assets, and your content. We expect a few rounds of revisions on these, and we'll dive a bit deeper as we get buy in on the design direction.  </p>
					
				</div>
			</li>
			
			<li class="tl-node completed">
				<h4 class="tl-stamp">Design: July  16, 2015</h4>
				<h4>Final Design Revisions completed</h4>
				<div class="tl-content">
						<p>As we wrap up the designs with your team, we are looking for a final approval on the design by this date. </p>
				</div>
			</li>
			
			<li class="tl-node development completed">
				<h4 class="tl-stamp">Development: July 16, 2015</h4>
				<h4>Staging server & Git Repositories set up</h4>
				<div class="tl-content">
						<p>We'll be setting up a staging server for the initial development of the server. <em>IF</em> factor1 is hosting the site this will be the final location of the site prior to the DNS switch.
						 This is ideal for the production environment. </p>
						 <a href="#"><i class="fa fa-check-circle"></i>  Completed July 10 - MA </a>
				</div>

			</li>
			
			
			<li class="tl-node development completed">
				<h4 class="tl-stamp">Development: July 16, 2015</h4>
				<h4>HTML / CSS initial construction begins</h4>
				<div class="tl-content">
						<p>From here we start coding out the site. We'll spend 3 - 5 days coding the shell templates and setting up infrastructure of the site CMS</p>
						<p>Depending on the feedback from the designs on Feb. 2, we will do as much here as we can. Coding out approved sections only</p>
						<a href="#"><i class="fa fa-check-circle"></i>  Completed July 14 - ES </a>
				</div>
			</li>

			
			<li class="tl-node development completed">
				<h4 class="tl-stamp">Development: July 23, 2015</h4>
				<h4>Advanced theme development wrapping up.</h4>
				<div class="tl-content">
					<a href="#"><i class="fa fa-check-circle"></i>  Completed July 17 - ES </a>
				</div>
			</li>
			
			<li class="tl-node development completed">
				<h4 class="tl-stamp">Development: July 24, 2015</h4>
				<h4>Alpha testing</h4>
				<div class="tl-content">
						<p>Using our in house testing tools, we will be running the site through a wide array of tools to test the site construction. 
						This includes Mac, Windows, iOS, Android, Windows Mobile, Performance load testing, and security tests.</p>
						<a href="#"><i class="fa fa-check-circle"></i>  Completed July 17 - ES </a>
				</div>
			</li>
			
						<li class="tl-node ">
				<h4 class="tl-stamp">Admin: July 23, 2015</h4>
				<h4>Begin content population</h4>
				<div class="tl-content">
						<p>Since we have content supplied to us a head of schedule we will be adding in content. This will help wrap up design and development as we identify places 
						where the content demands better layouts to suite the content and context at hand.</p>
				</div>
			</li>
			
			<li class="tl-node development">
				<h4 class="tl-stamp">Development: July 24, 2015</h4>
				<h4>Advanced theme development</h4>
				<div class="tl-content">
						<p>Wrapping up theme and content related site features</p>
				</div>
			</li>
			

			
			<li class="tl-node">
				<h4 class="tl-stamp">Admin: July 31, 2015</h4>
				<h4>Factor1 Alpha testing Complete</h4>
				<div class="tl-content">
						<p>Using our in house testing tools and your team, we will be running the site through a wide array of tools to test the site construction, content and registration process. This includes Mac, Windows, iOS, Android, Windows Mobile, Performance load testing, and security tests.</p>
				</div>
			</li>
			
			
			<li class="tl-node development">
				<h4 class="tl-stamp">Development: July 31, 2015</h4>
				<h4>DNAZ beta testing of Certification tool begins</h4>

			</li>
			
			<li class="tl-node">
				<h4 class="tl-stamp">Admin: August 3, 2015</h4>
				<h4>Content Entry Begins: Training</h4>
				<div class="tl-content">
						<p>Working with your team, we will begin basic training for initial static content. This includes importing any content we can, and training your team on the many custom tools we have built for you. We may also be ready for classes at this point. If not that will be a few days later.</p>
				</div>
			</li>
			

			
			
			<li class="tl-node critical">
				<h4 class="tl-stamp">Development: August 10th, 2015</h4>
				<h4>Content Entry completed</h4>
				<div class="tl-content">
						<p>Using the Quality Assurance list from the site kick off, we will have all tasks completed.</p>
						
						<a href="https://basecamp.com/1762986/projects/9758540/todolists/29359537" target="_blank">View QA Task List</a>
				</div>
			</li>
			
			<li class="tl-node critical">
				<h4 class="tl-stamp">Development: August 10th, 2015</h4>
				<h4>QA launch list completed</h4>
				<div class="tl-content">
						<p>Using the Quality Assurance list from the site kick off, we will have all tasks completed.</p>
						
						<a href="https://basecamp.com/1762986/projects/9758540/todolists/29359537" target="_blank">View QA Task List</a>
				</div>
			</li>
			
			<li class="tl-node critical">
				<h4 class="tl-stamp">Development: August 13th, 2015</h4>
				<h4>Online Certification tool ready for production</h4>
			</li>
			
			<li class="tl-node ">
				<h4 class="tl-stamp">Admin: August 10-14, 2015</h4>
				<h4>Final walk through punch list</h4>
				<div class="tl-content">
						<p>Here we need to have a final task list of known issues and desired changes outlined. Using a BaseCamp task list, we will map out all items that need our touch to wrap up for launch.</p>
						
				</div>
			</li>
			
			<li class="tl-node development">
				<h4 class="tl-stamp">August 17, 2015</h4>
				<h4>All DNAZ supplied feedback and beta punch list items completed</h4>
			</li>
			
			<li class="tl-node development">
				<h4 class="tl-stamp">August 18, 2015</h4>
				<h4>Site launch</h4>
				<div class="tl-content">
						<p>Site should be live!.</p>
						<a href="http://dnaz.org">View Site</a>
				</div>
			</li>
			
			

			
		</ol>
	</section>
	
	
	
	<footer>
		<div class="row">
			<p><span>factor1</span></p>
		</div>
	</footer>
</body>
</html>