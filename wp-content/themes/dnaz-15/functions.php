<?php

// Edit get_the_content() functionality
function get_the_content_with_formatting ($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
	$content = get_the_content($more_link_text, $stripteaser, $more_file);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

// Register Custom Post Type For Hospital Coordinators
function hospital_coordinators() {

	$labels = array(
		'name'                  => _x( 'Hospitals', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Hospital', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Hospitals', 'text_domain' ),
		'name_admin_bar'        => __( 'Hospitals', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Hospital:', 'text_domain' ),
		'all_items'             => __( 'All Hospitals', 'text_domain' ),
		'add_new_item'          => __( 'Add New Hospital', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Hospital', 'text_domain' ),
		'edit_item'             => __( 'Edit Hospital', 'text_domain' ),
		'update_item'           => __( 'Update Hospital', 'text_domain' ),
		'view_item'             => __( 'View Hospital', 'text_domain' ),
		'search_items'          => __( 'Search Hospitals', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'hospitals',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Hospital', 'text_domain' ),
		'description'           => __( 'Custom Post Types for Hospital Coordinators', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'hospital_coordinator', $args );

}
add_action( 'init', 'hospital_coordinators', 0 );

// Register Custom Post Type
function sidebar_stories() {

	$labels = array(
		'name'                => _x( 'Sidebar Stories', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Sidebar Story', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Sidebar Story', 'text_domain' ),
		'name_admin_bar'      => __( 'Sidebar Story', 'text_domain' ),
		'parent_item_colon'   => __( 'Sidebar Story:', 'text_domain' ),
		'all_items'           => __( 'All Sidebar Stories:', 'text_domain' ),
		'add_new_item'        => __( 'Add New Sidebar Story', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Sidebar Story', 'text_domain' ),
		'edit_item'           => __( 'Edit Sidebar Story', 'text_domain' ),
		'update_item'         => __( 'Update Sidebar Story', 'text_domain' ),
		'view_item'           => __( 'View Sidebar Story', 'text_domain' ),
		'search_items'        => __( 'Search Sidebar Story', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'sidebar_stories', 'text_domain' ),
		'description'         => __( 'Sidebar Storie Posts', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'sidebar_stories', $args );

}

// Hook into the 'init' action
add_action( 'init', 'sidebar_stories', 0 );

// Build the breadcrumb menus
function wordpress_breadcrumbs() {
 
  $delimiter = '<i class="fa fa-chevron-right"></i>';
  $name = 'Home'; //text for the 'Home' link
  $currentBefore = '<span class="current">';
  $currentAfter = '</span>';
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 
    echo '<div id="crumbs">';
 
    global $post;
    $home = get_bloginfo('url');
    echo '<a href="' . $home . '">' . $name . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $currentBefore . 'Archive by category &#39;';
      single_cat_title();
      echo '&#39;' . $currentAfter;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('d') . $currentAfter;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('F') . $currentAfter;
 
    } elseif ( is_year() ) {
      echo $currentBefore . get_the_time('Y') . $currentAfter;
 
    } elseif ( is_single() ) {
      $cat = get_the_category(); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_search() ) {
      echo $currentBefore . 'Search results for &#39;' . get_search_query() . '&#39;' . $currentAfter;
 
    } elseif ( is_tag() ) {
      echo $currentBefore . 'Posts tagged &#39;';
      single_tag_title();
      echo '&#39;' . $currentAfter;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $currentBefore . 'Articles posted by ' . $userdata->display_name . $currentAfter;
 
    } elseif ( is_404() ) {
      echo $currentBefore . 'Error 404' . $currentAfter;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
}
//add an options page
if( function_exists('acf_add_options_page') ) {
 
	$page = acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title' 	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
	$page = acf_add_options_page(array(
		'page_title' 	=> 'Newsletter Archives',
		'menu_title' 	=> 'Newsletter Archives',
		'menu_slug' 	=> 'newsletter-archives',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
 
}

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );
	
// Lets make some shortcodes
include(get_template_directory().'/shortcode_maker.php');

// Strip P and BR tags for shortcodes
/*
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);
*/

	
// Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}
	
//Customize Wordpress Admin

	// add login logo
	function custom_loginlogo() {
	echo '<style type="text/css">
	h1 a {
		height: 100% !important; 
		width:100% !important;
		background-image: url('.get_bloginfo('template_directory').'/images/logo-login.png) !important;
		background-postion-x: center !important;
		background-size:100% !important; 
		margin-bottom:10px !important; }
	
	h1 {
		width: 320px !important; 
		Height: 120px !important}
		
	</style>';
	}
	
	add_action('login_head', 'custom_loginlogo');
	
	
	// add custom footer text
	function modify_footer_admin () {
		echo 'Created by <a href="http://factor1studios.com"><strong>factor1</strong></a>. ';
		echo 'Powered by<a href="http://WordPress.org">WordPress</a>.';
		}

	add_filter('admin_footer_text', 'modify_footer_admin');

/// Dump the yoast SEO columns that are ugly and messy
add_filter( 'wpseo_use_page_analysis', '__return_false' );
	
// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    
// Remove WP version from html header  
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
   
    
//  Stop automatically hyperlinking images to themselves
$image_set = get_option( 'image_default_link_type' );
    if (!$image_set == 'none') {
        update_option('image_default_link_type', 'none');
    }
        
// add theme supports and nav menus
	add_action( 'after_setup_theme', 'f1_setup' );
	function f1_setup() {
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );

	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'start_post_rel_link' );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'adjacent_posts_rel_link' );

	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'f1' ),
		'about-us' => __( 'About Us Mega Menu', 'f1' ),
		'understanding-donation' => __( 'Understanding Donation Mega Menu', 'f1' ),
		'donor-families' => __( 'Donor Families Mega Menu', 'f1' ),
		'transplant-recipients' => __( 'Transplant Recipients Mega Menu', 'f1' ),
		'professional-partners' => __( 'Professional Partners Mega Menu', 'f1' ),
		'get-involved' => __( 'Get Involved Mega Menu', 'f1' ),
		'utility' => __( 'Utility Navigation', 'f1' ),
		'footer-nav' => __( 'Footer Navigation', 'f1' ),
	) );
}



// add page excerpts
	function page_excerpt() {
	add_post_type_support('page', array('excerpt'));
	}
	add_action('init', 'page_excerpt');
	


// register widget areas
	add_action( 'widgets_init', 'f1_widgets_init', 1 );
	function f1_widgets_init() {
	unregister_widget( 'WP_Widget_Calendar' );
	unregister_widget( 'WP_Widget_Links' );
	unregister_widget( 'WP_Widget_Meta' );
	unregister_widget( 'WP_Widget_Search' );
	unregister_widget( 'WP_Widget_Recent_Comments' );

	register_sidebar( array(
		'name' => __( 'Sidebar', 'f1' ),
		'id' => 'sidebar',
		'description' => __( 'The primary widget area on the right side', 'f1' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Sidebar Blog', 'f1' ),
		'id' => 'sidebar_blog',
		'description' => __( 'The primary widget area on the right side', 'f1' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );

}

// change default "read more" link
	function f1_continue_reading_link() {
	return '';
	}

// change default [...]
	add_filter( 'excerpt_more', 'f1_auto_excerpt_more' );
	function f1_auto_excerpt_more( $more ) {
	return ' &hellip;' . f1_continue_reading_link();
	}

// remove the gallery style that is automatically added
	add_filter( 'gallery_style', 'f1_remove_gallery_css' );
	function f1_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
	}

// remove the recent comments style that is automatically added
	add_action( 'widgets_init', 'f1_remove_recent_comments_style' );
	function f1_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'  ) );
	}

// remove update notifications for everybody except admin users
	global $user_login;
	get_currentuserinfo();
	if ( ! current_user_can( 'update_plugins' ) ) { // checks to see if current user can update plugins 
	add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
	add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
	}

// remove dashboard boxes
	add_action( 'admin_menu', 'f1_remove_dashboard_boxes' );
	function f1_remove_dashboard_boxes() {
        // remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' ); // Right Now Overview Box
        // remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' ); // Incoming Links Box
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' ); // Quick Press Box
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' ); // Plugins Box
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' ); // Recent Drafts Box
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'core' ); // Recent Comments
        remove_meta_box( 'dashboard_primary', 'dashboard', 'core' ); // WordPress Development Blog
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' ); // Other WordPress News
}

// remove meta boxes from default posts screen
	add_action('admin_menu','f1_remove_default_post_metaboxes');
	function f1_remove_default_post_metaboxes() {
        remove_meta_box( 'postcustom','post','normal' ); // Custom Fields Metabox
        remove_meta_box( 'postexcerpt','post','normal' ); // Excerpt Metabox
        remove_meta_box( 'commentstatusdiv','post','normal' ); // Comments Metabox
        remove_meta_box( 'trackbacksdiv','post','normal' ); // Talkback Metabox
        //remove_meta_box( 'authordiv','post','normal' ); // Author Metabox
}

// remove meta boxes from default pages screen
	add_action('admin_menu','f1_remove_default_page_metaboxes');
	function f1_remove_default_page_metaboxes() {
	remove_meta_box( 'postcustom','page','normal' ); // Custom Fields Metabox
	//remove_meta_box( 'commentstatusdiv','page','normal' ); // Discussion Metabox
	//remove_meta_box( 'authordiv','page','normal' ); // Author Metabox
}

?>