<?php /* Template Name: All Events */
get_header();
?>

<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
	</div>
</div>


<section class="row page-content-container">
	<div class="medium-8 columns">
		<?php
		/**
		 * Query Upcoming Events
		 */
		$arrRecentEvents = array();
		$today = date('Y-m-d');
		$args = array(
		    'post_type' => 'event',
		    'posts_per_page' => 3,
		    'orderby' => 'meta_value',
		    'meta_key' => '_event_start_date',
		    'order' => 'ASC',
		    'meta_query' => array(
		        array(
		            'key' => '_event_start_date',
		            'value' => $today,
		            'compare' => '>=',
		           
		        )
		    )
		);
		$query = new WP_Query($args);
		if ($query->have_posts()) {
		    while($query->have_posts()) {
		      $query->the_post();
		      $post->category = wp_get_post_terms($post->ID.'event-categories');
		      $arrRecentEvents[] = $post; // add post to recent posts array
		    }
		}
		
		//If we have events	
		if (!empty($arrRecentEvents)): ?>
		
		<ul class="upcoming-events">
		
		<?php  //For Each Event Do This
		global $post; ?>
		<?php foreach ($arrRecentEvents as $i => $post): $EM_Event = em_get_event($post->ID, 'post_id'); ?>
			<li>
				<div class="row">
					<div class="medium-4 text-center columns">
						<div class="date-box">
							<span class="month"><?php echo date('F');?></span>
							<hr>
							<span class="day"><?php echo date('j');?></span>
						</div>
					</div>
					<div class="medium-8 columns">
						<p class="event-title"><?php the_title();?></p>
						<?php echo $EM_Event->output('#_EVENTNOTES');?>
						<a href="<?php the_permalink();?>" class="button button-green">Event Details</a>
					</div>
				</div>
			</li>
		<?php endforeach; wp_reset_query();?>
		
		</ul>
		<?php endif;?>
	</div>
	
	<aside class="medium-4 columns">
		<?php get_sidebar();?>
	</aside>
</section>


<?php get_footer();?>