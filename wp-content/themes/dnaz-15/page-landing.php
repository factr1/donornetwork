<?php /* Template Name: Landing Page */ 
get_header();
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
$url = $thumb['0']; 
?>

<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header hide-for-small-only" style="background: url(<?=$url?>) center top no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
	
	<!-- Mobile Hero w/ image -->
	<section class="page-header show-for-small-only">
		<img src="<?=$url?>" alt="Donor Network of Arizona image">
		<div class="content-container">
			<h1>
				<?php the_title();?>
			</h1>
			<?php if(get_field('page_description')): the_field('page_description'); endif;?>
		</div>
	</section>
	
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
	</div>
</div>

<section class="row page-content-container">
	<div class="medium-10 columns medium-centered">
		<?php if(get_field('landing_header')):?>
		<h2 class="no-line">
			<?php the_field('landing_header');?>
		</h2>
		<?php endif;?>
	</div>
	
	<!-- Image Grid -->
	<?php if(have_rows('landing_image_blocks')):?>
		<ul class="small-block-grid-1 medium-block-grid-2">
			<?php while(have_rows('landing_image_blocks')): the_row();?>
				<li>
					<div class="landing-image-block" style="background: url(<?php the_sub_field('landing_image');?>) center center no-repeat;">
					</div>
				</li>
			<?php endwhile;?>
		</ul>
	<?php endif;?>
	
	<!-- Main Content -->
	<?php if(have_posts()): while(have_posts()): the_post();?>
		<article class="medium-10 columns medium-centered">
			<?php the_content();?>
		</article>
	<?php endwhile; endif;?>
	
	<!-- Icon Blocks -->
	<?php if(have_rows('icon_blocks')):?>
		<ul class="icon-blocks">
			<?php while(have_rows('icon_blocks')): the_row();?>
				<li class="text-center">
					<img src="<?php the_sub_field('icon_image');?>">
					<h3>
						<?php the_sub_field('block_title');?>
					</h3>
					
						<p><?php the_sub_field('block_content');?></p>
					
					<a href="<?php the_sub_field('read_more_link');?>" class="button button-green">
						<?php the_sub_field('read_more_text');?>
					</a>
				</li>
			<?php endwhile;?>
		</ul>
	<?php endif;?>
</section>

<script>
var maxHeight = 0;
	
$('.icon-blocks li').each(function(){
	if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
});

$('.icon-blocks li').height(maxHeight);
</script>

<?php get_footer();?>