<?php /* Template Name: Our History */
	get_header();?>

<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
	</div>
</div>

<section class="row page-content-container">
	<div class="medium-10 medium-centered columns">
	<?php if(have_rows('timeline')):?>
		<ol class="timeline">
			<?php while(have_rows('timeline')): the_row();?>
				<li>
					<div class="timeline-date">
						<span><?php the_sub_field('timeline_date');?></span>
					</div>
					<div class="row">
						<?php if(get_sub_field('show_timeline_photo') == true):
							$leftcol = 'small-12 medium-3';
							$rightcol = 'small-12 medium-9';
							
							else:
							$leftcol = 'hide';
							$rightcol = 'small-12';
						endif;?>
						
						<!--left column -->
						<div class="<?php echo $leftcol;?> columns text-center">
							<img src="<?php the_sub_field('timeline_photo');?>" alt="Timeline photograph">
						</div>
						<!-- right column -->
						<div class="<?php echo $rightcol;?> columns">
							<h2>
								<?php the_sub_field('timeline_headline');?>
							</h2>
							<p>
								<?php the_sub_field('timeline_content');?>
							</p>
						</div>
					</div>
				</li>
			<?php endwhile;?>
		</ol>
	<?php endif;?>
	</div>
</section>

<?php get_footer();?>