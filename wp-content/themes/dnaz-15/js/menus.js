//lets start by preloading the mega bg image
function preload(arrayOfImages) {
    $(arrayOfImages).each(function() {
        $('<img/>')[0].src = this;
    });
}

//global state for the nav
var open = false;

//big function to remove active class when leaving tab
var removeActive = function() {
    $('.menu-item-138, .menu-item-139, .menu-item-140, .menu-item-141, .menu-item-142, .menu-item-143, .search-nav').removeClass('active-tab');
};

//open about nav
var openAboutNav = function() {
    $('.mega-menu').slideUp();
    $('.about-us').slideDown(250, function() {
        open = true;
    });
};


//open prof partners nav
var openPartners = function() {
    $('.mega-menu').slideUp();
    $('.professional-partners').slideDown(250, function() {
        open = true;
    });
};

//open understanding donation nav
var openUnderstanding = function() {
    $('.mega-menu').slideUp();
    $('.understanding-donation').slideDown(250, function() {
        open = true;
    });
};

//open donor families nav
var openDonor = function() {
    $('.mega-menu').slideUp();
    $('.donor-families').slideDown(250, function() {
        open = true;
    });
};

//open transplant recipients nav
var openTransplant = function() {
    $('.mega-menu').slideUp();
    $('.transplant-recipients').slideDown(250, function() {
        open = true;
    });
};

//open get involved nav
var openGetInvolved = function() {
    $('.mega-menu').slideUp();
    $('.get-involved').slideDown(250, function() {
        open = true;
    });
};

//push all our mouse enters to one function 
var hovers = function() {
    $('.menu-item-138').mouseenter(function() {
        removeActive();
        openAboutNav();
        $(this).addClass('active-tab');
    });

    $('.menu-item-139').mouseenter(function() {
        removeActive();
        openUnderstanding();
        $(this).addClass('active-tab');
    });

    $('.menu-item-140').mouseenter(function() {
        removeActive();
        openDonor();
        $(this).addClass('active-tab');
    });

    $('.menu-item-141').mouseenter(function() {
        removeActive();
        openTransplant();
        $(this).addClass('active-tab');
    });

    $('.menu-item-142').mouseenter(function() {
        removeActive();
        openPartners();
        $(this).addClass('active-tab');
    });

    $('.menu-item-143').mouseenter(function() {
        removeActive();
        openGetInvolved();
        $(this).addClass('active-tab');
    });

    $('.search-nav').mouseenter(function() {
        removeActive();
        $(this).addClass('active-tab');
    });
};

//mouse out, slide up the mega nav globally
var mouseOutClose = function() {
    $('.mega-menu').mouseleave(function() {
        $(this).slideUp(150, function() {
            open = false;
        });
        removeActive();
    });
};

//if we hover to the north of the nav 
var headerHover = function() {
    $('.header-container').mouseenter(function() {
        $('.mega-menu').slideUp(150, function() {
            open = false;
        });
        removeActive();
    });
};

var searchRow = $('.search-row');

//open search function
var openSearch = function() {
    $('.search-row').css('display', 'block');
    $('.search-box').animate({
        opacity: 1
    }, 400);
    //when you click outside
    $(document).on('click', function(event) {
	  if (!$(event.target).closest('.search-box').length) {
	    closeSearch();
	  }
	});
};

var closeSearch = function() {
    $('.search-box').animate({
        opacity: 0
    }, 400);
    setTimeout( function(){
	    searchRow.css('display', 'none')
	    }, 400);
    removeActive();
};

//sidebar children pages menu
var childrenPages = function(){
	var hasChildren = $('.sidebar .page_item_has_children > a');
	var i = 0;
	
	$(hasChildren).addClass('sidebar-caret');
	
	$(hasChildren).removeAttr('href');
	
	$('.sidebar .page_item_has_children').click(function(){
		i++;
		$('ul', this).toggle(1);
		$('a',this).toggleClass('sidebar-caret-down');
		
	});	
};


//DOCUMENT READY
//listen for the hovers
$(document).ready(function() {
	
	//sidebar children pages - ready
    childrenPages();

    //search options/functions
    $('#menu-primary-menu-1').append('<li class="menu-item menu-item-type-post_type menu-item-object-page search-nav"> <a class="search-open"><i class="fa fa-search"></i></a> </li>');

    $('.search-open').mouseenter(function() {
        openSearch();
    });

    var viewport = $(window).innerWidth();

    if (viewport > 768) {
        preload([
            'http://www.dnaz.org/wp-content/themes/dnaz-15/images/megamenu.png',
        ]);

        //listen for when the mouse leaves
        mouseOutClose();
        headerHover();

        //listen for when the mouse enters nav items
        hovers();
    } else {
        console.log('Hi mobile user!');
        return;
    }
});