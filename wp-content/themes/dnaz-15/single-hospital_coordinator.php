<?php get_header();?>

<?php
$the_query = new WP_Query( 'page_id=64' );
	while ( $the_query->have_posts() ) :
	$the_query->the_post();

		$page_content = get_the_content_with_formatting();

endwhile;
wp_reset_postdata();
?>

<div class="row">

	<?php if(have_posts()):?>
		<div class="medium-8 columns">
			<?php while(have_posts()): the_post();?>

				<?php echo $page_content;?>
				

				<h1><?php the_title();?></h1>
				<?php
				/*
				*  Query posts for a relationship value.
				*  This method uses the meta_query LIKE to match the string "123" to the database value a:1:{i:0;s:3:"123";} (serialized array)
				*/

				$coordinators = get_posts(array(
					'post_type' => 'f1_staffgrid_cpt',
					'meta_query' => array(
						array(
							'key' => 'select_hospitals', // name of custom field
							'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
							'compare' => 'LIKE'
						)
					)
				));

				if( $coordinators ): ?>
				<ul class="program-coordinator">
				<?php foreach( $coordinators as $coordinators ): ?>
					<li>

						<strong><?php echo get_the_title( $coordinators->ID ); ?></strong>
						<br>
						<?php echo get_field('title', $coordinators->ID);?>
						<br>
						<a href="mailto:<?php echo get_field('email_address', $coordinators->ID);?>">
							<i class="fa fa-envelope"></i> <?php echo get_field('email_address', $coordinators->ID);?>
						</a>
						<br>
						<i class="fa fa-phone"></i> <?php echo get_field('phone', $coordinators->ID);?>

					</li>
				<?php endforeach; ?>
				</ul>
			<?php endif; ?>


			<?php endwhile;?>
		</div>
	<?php endif;?>

	<aside class="medium-4 columns" style="margin-top: 30px;">
		<?php get_sidebar(); ?>
	</aside>

</div>

<?php get_footer();?>
