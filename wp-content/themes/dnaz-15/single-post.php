<?php 
	get_header();
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0']; 
?>

<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					Blog
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
	</div>
</div>

<section class="row page-content-container">
	<article class="medium-8 columns">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				
				<?php //include (TEMPLATEPATH . '/inc/meta.php' ); ?>
				
				<?php if(has_post_thumbnail()):?>
					<div class="featured-image-blog">
						<?php the_post_thumbnail('large');?>
					</div>
				<?php endif;?>
				
				<div class="post-title">
					<h2><?php the_title();?></h2>
					<span><?php the_time('m/j/Y');?></span>
				</div>
			
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
	
	
				
				<?php edit_post_link('Edit this entry','','.'); ?>
			</div>
			
		<?php endwhile; endif; ?>
		
		<div class="navigation">
			<div class="alignleft">
				<?php previous_post('&lt; %',
					'Previous ', 'no'); ?>
			</div>
			<div class="alignright">
				<?php next_post('% &gt; ',
					'Next ', 'no'); ?>
			</div>
		</div> <!-- end navigation -->
		
	</article>
	<aside class="medium-4 columns">
		<?php get_sidebar('blog'); ?>
	</aside>
</section>

<?php get_footer(); ?>