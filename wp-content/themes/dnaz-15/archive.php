<?php 
	get_header();
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0']; 
?>

	<!-- If we are showing an image header -->
	<?php if(get_field('show_header') == true):?>
		<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
			<div class="slide-content row">
				<div class="medium-10 medium-centered content-container columns">
					<h1>
						<?php if (have_posts()) : ?>

			 			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			
						<?php /* If this is a category archive */ if (is_category()) { ?>
							<h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>
			
						<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
							<h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>
			
						<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
							<h2>Archive for <?php the_time('F jS, Y'); ?></h2>
			
						<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
							<h2>Archive for <?php the_time('F, Y'); ?></h2>
			
						<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
							<h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>
			
						<?php /* If this is an author archive */ } elseif (is_author()) { ?>
							<h2 class="pagetitle">Author Archive</h2>
			
						<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
							<h2 class="pagetitle">Blog Archives</h2>
						
						<?php } ?>
						
						<?php endif;?>
					</h1>
					<?php if(get_field('page_description')): the_field('page_description'); endif;?>
				</div>
			</div>
		</section>
	<?php else:?>
		<section class="page-header-no-image">
			<div class"row">
				<div class="medium-10 columns medium-centered text-center">
						<?php if (have_posts()) : ?>

			 			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			
						<?php /* If this is a category archive */ if (is_category()) { ?>
							<h1>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
			
						<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
							<h1>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
			
						<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
							<h1>Archive for <?php the_time('F jS, Y'); ?></h1>
			
						<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
							<h1>Archive for <?php the_time('F, Y'); ?></h1>
			
						<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
							<h1 class="pagetitle">Archive for <?php the_time('Y'); ?></h1>
			
						<?php /* If this is an author archive */ } elseif (is_author()) { ?>
							<h1 class="pagetitle">Author Archive</h1>
			
						<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
							<h1 class="pagetitle">Blog Archives</h1>
						
						<?php } ?>
						
						<?php endif;?>
					<?php if(get_field('page_description')): the_field('page_description'); endif;?>
				</div>
			</div>
		</section>
	<?php endif;?>
	
	<div class="page-navigation">
		<div class="row">
			<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
		</div>
	</div>

<article class="row page-content-container">
	<div class="medium-8 columns">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?>>
			<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>


			<?php the_content(); ?>


		</div>

		<?php endwhile; ?>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
			
		<?php else : ?>

			<h2>Nothing found</h2>

			<?php endif; ?>
	</div>
	<aside class="medium-4 columns">
		<?php get_sidebar(); ?>
	</aside>
</article>



<?php get_footer(); ?>
