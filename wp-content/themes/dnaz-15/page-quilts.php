<?php /* Template Name: Image Grid */
	get_header();
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0']; 
?>

	<!-- If we are showing an image header -->
	<?php if(get_field('show_header') == true):?>
		<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
			<div class="slide-content row">
				<div class="medium-10 medium-centered content-container columns">
					<h1>
						<?php the_title();?>
					</h1>
					<?php if(get_field('page_description')): the_field('page_description'); endif;?>
				</div>
			</div>
		</section>
	<?php else:?>
		<section class="page-header-no-image">
			<div class"row">
				<div class="medium-10 columns medium-centered text-center">
					<h1>
						<?php the_title();?>
					</h1>
					<?php if(get_field('page_description')): the_field('page_description'); endif;?>
				</div>
			</div>
		</section>
	<?php endif;?>
	
	<div class="page-navigation">
		<div class="row">
			<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
		</div>
	</div>
	
	<section class="row page-content-container">
		<div class="medium-8 columns">
			<?php if(have_posts()): while(have_posts()): the_post();?>
				<article>
					<?php the_content();?>
					
					<!-- Donor Quilt Gallery -->
					<?php if(have_rows('quilts')):?>
						<ul class="small-block-grid-1 medium-block-grid-3">
							<?php while(have_rows('quilts')): the_row();?>
								<li>
									<a href="<?php the_sub_field('quilt_image');?>" class="swipebox" rel="quilt-gallery" title="<?php the_sub_field('quilt_caption');?>">
										<img src="<?php the_sub_field('quilt_image');?>" alt="Donor Quilt">
									</a>
								</li>
							<?php endwhile;?>
						</ul>
					<?php endif;?>
					
				</article>
			<?php endwhile; endif;?>
		</div>
		<div class="medium-4 columns">
			<?php get_sidebar();?>
		</div>
	</section>




<?php get_footer(); ?>
