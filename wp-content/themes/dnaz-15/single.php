<?php 
	get_header();
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0']; 
?>

<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php //if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
	</div>
</div>

<section class="row page-content-container">
	<article class="medium-8 columns">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				
				<?php //include (TEMPLATEPATH . '/inc/meta.php' ); ?>
				
	
				<?php if(has_post_thumbnail()) {
				the_post_thumbnail('large');
				} else {	}
				?>	
				
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
				<?php the_tags( 'Tags: ', ', ', ''); ?>
	
	
				
				<?php edit_post_link('Edit this entry','','.'); ?>
				
			</div>
	
		<?php //comments_template(); ?>
	
		<?php endwhile; endif; ?>
	</article>
	<aside class="medium-4 columns">
		<?php get_sidebar('blog'); ?>
	</aside>
</section>

<?php get_footer(); ?>