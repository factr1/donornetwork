<?php get_header(); ?>
<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					Blog
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					Blog
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php 
			if(is_home()):
				echo '<div id="crumbs"><a href="http://beta.dnaz.org">Home</a> <i class="fa fa-chevron-right"></i> <a href="http://beta.dnaz.org/newsroom/">Newsroom</a> <i class="fa fa-chevron-right"></i> <span class="current">Blog</span></div>';
			else:
				if (function_exists('wordpress_breadcrumbs')): 
					wordpress_breadcrumbs(); 
				endif;
			endif;
		?>
	</div>
</div>

	<section class="row page-content-container">
		<div class="medium-8 columns" style="margin-top: 19px;">
			<?php if(have_posts()): while(have_posts()): the_post();?>
			<div class="row" style="margin-bottom: 30px;">
				<?php if(has_post_thumbnail()):?>
					<div class="small-4 columns">
						<?php the_post_thumbnail('medium');?>
					</div>
				<?php endif;?>
					<article class="<?php if(has_post_thumbnail()): echo 'small-8'; else: echo 'small-12'; endif;?> columns post-index">
						<h2 style="padding-top: 0;"><?php the_title();?></h2>
						<?php the_excerpt();?>
						<a href="<?php the_permalink();?>" class="button">Read More</a>
					</article>
			</div>
			<?php endwhile; endif;?>
		</div>
		<div class="medium-4 columns">
			<?php get_sidebar('blog');?>
		</div>
	</section>

<?php get_footer(); ?>