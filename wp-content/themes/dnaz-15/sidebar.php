<div class="sidebar">

    <?php 
	    //$sidebar = false;
	    if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar')) :  else :
	    	echo 'Nothing Here!'; 
		endif; ?>
	

		
	<div class="case-study" style="<?php //if($sidebar == false): echo 'margin-top: 0;'; endif;?>">
		<?php // WP_Query arguments
		$args = array (
			'post_type'              => array( 'sidebar_stories' ),
			'posts_per_page'         => '1',
			'order'                  => 'DESC',
			'orderby'                => 'rand',
		);
		
		// The Query
		$query = new WP_Query( $args );
		?>
		
		<?php if($query->have_posts()): while($query->have_posts()): $query->the_post();?>
		<?php the_post_thumbnail();?>
		<h2><?php the_title();?></h2>
		
		<?php the_content();?>
		
		<a href="<?php the_field('click_url');?>" class="button button-green" target="_blank"><?php the_field('button_text');?></a>
		
		<?php endwhile; endif; wp_reset_postdata();?>
	</div>

</div>

<script>

	var childrenPresent = false;
	
	
	$('.sidebar div').each(function(){
		if( $(this).hasClass('widget_children_pages') ){
			childrenPresent = true;
			
		} else{
			if(childrenPresent == false){
				$('.case-study').css('margin-top','0');
			}
		}
	});
	
</script>