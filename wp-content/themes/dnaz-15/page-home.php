<?php /* Template Name: Home */
get_header();
?>

<!-- Home Slider -->
<?php if(have_rows('home_slider')):?>
	<section class="home-slider">
		<?php while(have_rows('home_slider')): the_row();?>
			<div class="home-slide" style="background: url(<?php the_sub_field('slide_image');?>) top center no-repeat;">
				<div class="slide-content row">
					<div class="medium-10 medium-centered content-container columns">
						<?php the_sub_field('slide_content');?>
					</div>
				</div>
			</div>
		<?php endwhile;?>
	</section>
<?php endif;?>

<!--decorative gradient-->
<div class="gradient"></div>



<section class="top-box row">
	<div class="medium-6 columns text-center">
		<p class="counter">
			<?php 
			$count = file_get_contents('https://www.donatelifeaz.org/wp-content/themes/twentytwelve/count.txt');
			echo $count;
			?>
		</p>
		<p class="counter-text">people have said "yes" to hope and generosity by registering as organ, eye and tissue donors in Arizona.</p>
		<a href="<?php bloginfo('url');?>/about-donation/" class="button button-green">About Donation</a>
		<a href="https://register.donatelifeaz.org/register/?lang=en" class="button button-green">Be a Donor <i class="fa fa-arrow-right"></i></a>
	</div>
	<div class="medium-6 columns">
		<?php the_field('top_box_content');?>
		<a href="<?php the_field('top_box_button_link');?>" class="button">
			<?php the_field('top_box_button_text');?>
		</a>
	</div>
</section>

<section class="box-cta">
	<div class="row">
		<?php if(have_rows('box_cta')):?>
			<ul class="small-block-grid-1 medium-block-grid-3">
				<?php while(have_rows('box_cta')): the_row();?>
					<li>
						<div class="box-container">
							<a href="<?php the_sub_field('box_link');?>">
								<div class="box-image" style="background: url(<?php the_sub_field('box_image');?>) center top no-repeat;"></div>
							</a>
							<a href="<?php the_sub_field('box_link');?>" class="row">
								<div class="small-10 medium-8 columns">
									<?php the_sub_field('box_link_text');?>
								</div>
								<div class="small-2 medium-4 text-right columns">
									<i class="fa fa-arrow-circle-right"></i>
								</div>
							</a>
						</div>
					</li>
				<?php endwhile;?>
			</ul>
		<?php endif;?>
	</div>
</section>

<section class="events-newsroom row">
	<div class="small-12 small-text-center medium-6 medium-text-left columns">
		<h3>Upcoming Events</h3> <a href="<?php bloginfo('url');?>/get-involved/calendar/" class="button button-white" style="margin-right: 40px;">See All Events</a>
		<br>

		<?php
		/**
		 * Query Upcoming Events
		 */
		$arrRecentEvents = array();
		$today = date('Y-m-d');
		$args = array(
		    'post_type' => 'event',
		    'posts_per_page' => 3,
		    'orderby' => 'meta_value',
		    'meta_key' => '_event_start_date',
		    'order' => 'ASC',
		    'meta_query' => array(
		        array(
		            'key' => '_event_start_date',
		            'value' => $today,
		            'compare' => '>=',
		           
		        )
		    )
		);
		$query = new WP_Query($args);
		if ($query->have_posts()) {
		    while($query->have_posts()) {
		      $query->the_post();
		      $post->category = wp_get_post_terms($post->ID.'event-categories');
		      $arrRecentEvents[] = $post; // add post to recent posts array
		    }
		}
		
		//If we have events	
		if (!empty($arrRecentEvents)): ?>
		
		<ul class="upcoming-events">
		
		<?php  //For Each Event Do This
		global $post; ?>
		<?php foreach ($arrRecentEvents as $i => $post): $EM_Event = em_get_event($post->ID, 'post_id'); ?>
			<li class="equalize">
				<div class="row">
					<div class="medium-4 text-center columns">
						<div class="date-box">
							<span class="month"><?php the_time('F');?></span>
							<hr>
							<span class="day"><?php the_time('j');?></span>
						</div>
					</div>
					<div class="medium-8 columns">
						<p class="event-title text-left"><?php the_title();?></p>
						<div class="text-left">
							<?php echo $EM_Event->output('#_EVENTNOTES');?>
						</div>
						<a href="<?php the_permalink();?>" class="button button-green">Event Details</a>
					</div>
				</div>
			</li>
		<?php endforeach; wp_reset_query();?>
		
		</ul>
		<?php endif;?>
	</div>
	<div class="small-12 small-text-center medium-6 medium-text-left columns from-newsroom">
		<h3>From The Newsroom</h3> <a href="<?php bloginfo('url');?>/newsroom/" class="button button-white">See All News</a>
		<br>
		
		<?php 
		// WP_Query arguments
		$args = array (
			'category_name'          => 'featured',
			'pagination'             => false,
			'posts_per_page'         => '3',
		);
		
		// The Query
		$query = new WP_Query( $args );
		
		if($query -> have_posts()):
		?>
			<ul class="newsroom-featured">
				<?php while($query -> have_posts() ): $query -> the_post();?>
					<li class="equalize">
						<div class="row">
							<div class="medium-12 columns">
								<p class="event-title text-left"><?php the_title();?></p>
								<div class="text-left">
									<?php the_excerpt(); ?>
								</div>
								<a href="<?php the_permalink();?>" class="button button-green">Read More</a>
							</div>
						</div>
					</li>
				<?php endwhile;?>
			</ul>
		<?php endif; wp_reset_postdata(); ?>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function(){
  $('.home-slider').slick({
    autoplay: true,
    arrows: true,
    dots: false,
    autoplaySpeed: 8000
  });
});

var maxHeight = 0;
	
$('.equalize').each(function(){
	if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
});

$('.equalize').height(maxHeight);
</script>

<?php get_footer();?>