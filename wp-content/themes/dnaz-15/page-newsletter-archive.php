<?php /* Template Name: Page with Newsletter Archives */
get_header();
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
$url = $thumb['0']; 
?>

<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header" style="background: url(<?=$url?>) center top no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
	</div>
</div>

<section class="row page-content-container">
	<div class="medium-8 columns">
		<?php if(have_posts()): while(have_posts()): the_post();?>
			<article>
				<?php the_content();?>
			</article>
			
			<?php if(have_rows('newsletter_archives','option')):?>
				<div class="row" style="margin-bottom: 45px;">
					<?php while(have_rows('newsletter_archives','option')): the_row();?>
						<?php if(get_sub_field('show_newsletter_thumbnail','option') == true):?>
							<div class="small-4 columns">
								<img src="<?php the_sub_field('newsletter_thumbnail','option');?>">
							</div>
						<?php endif;?>
							<div class="<?php if( get_sub_field('show_newsletter_thumbnail','option') == true ): echo 'small-8'; else: echo 'small-12'; endif;?> columns post-index">
								<h2 style="padding-top: 0;"><?php the_sub_field('newsletter_title','option');?></h2>
								<p><?php the_sub_field('newsletter_description','option');?></p>
								<a href="<?php the_sub_field('newsletter_link','option');?>" target="_blank" class="button">Read More</a>
							</div>
					<?php endwhile;?>
				</div>
			<?php endif;?>
			
			<?php if(get_field('archived_newsletters','option')):?>
			<div class="row">
				<div class="small-12 columns">
					<h2>Archived Newsletters</h2>
					<?php the_field('archived_newsletters','option');?>
				</div>
			</div>
			<?php endif;?>
			
		<?php endwhile; endif;?>
	</div>
	<div class="medium-4 columns">
		<?php get_sidebar();?>
	</div>
</section>

<?php get_footer();?>