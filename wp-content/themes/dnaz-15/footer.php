<footer>
	<div class="row">
		<div class="medium-3 columns text-center">
			<a href="<?php bloginfo('url');?>/">
				<img src="<?php bloginfo('template_url');?>/images/footer-logo.png" alt="Donor Network of Arizona logo">
			</a>
		</div>
		<div class="medium-5 columns copyright">
			<?php the_field('footer_copyright', 'option');?>
		</div>
		<div class="medium-4 columns text-right footer-nav">
			<?php wp_nav_menu(array('theme_location' => 'footer-nav'));?>
		</div>
	</div>
</footer>

<div class="site-credit">
	<div class="row">
		<div class="medium-3 right text-right columns">
			Site by <span><a href="http://factor1studios.com" target="_blank">factor1</a></span>
		</div>
	</div>
</div>

<script src="<?php bloginfo('template_url');?>/js/outdatedbrowser.min.js"></script>
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url');?>/js/slick.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/f1-nav.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/jquery.swipebox.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/menus.min.js"></script>




<script type="text/javascript">
;( function( $ ) {

	$( '.swipebox' ).swipebox();

} )( jQuery );
</script>

<div id="outdated"></div>

<!-- Outdated Browser JS -->
<script>
	//event listener: DOM ready
function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}
//call plugin function after DOM ready
addLoadEvent(function(){
    outdatedBrowser({
        bgColor: '#f25648',
        color: '#ffffff',
        lowerThan: 'boxShadow',
        languagePath: 'en.html'
    })
});
</script>

</body>
</html>