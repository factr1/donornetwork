<?php get_header(); ?>

<section class="row">
	<div class="medium-10 columns text-center medium-centered" style="padding: 80px 0 200px;">
		<h1>Sorry, but we can't find that page</h1>
		<h3>Try going back or go <a href="<?php bloginfo('url');?>/">home</a>.</h3>
	</div>
</section>

<?php get_footer(); ?>
