<div class="sidebar">
	<div class="recent-stories">
		<h2>Recent Stories</h2>
		
		<?php
			// WP_Query arguments
			$args = array (
				'pagination'             => false,
				'posts_per_page'         => '3',
			);
			
			// The Query
			$query = new WP_Query( $args );
			
			if($query->have_posts()): while($query->have_posts()): $query -> the_post();
			?>
				<div class="row">
					<div class="small-12 columns recent-blog-post">
						<a href="<?php the_permalink();?>">
							<?php the_title();?>
							<br>
							<span class="sidebar-meta"><?php the_time('m/j/Y');?></span>
						</a>
					</div>
				</div>
			<?php endwhile; endif; wp_reset_postdata();?>
	</div>
	
	<div class="email-updates">
		<p>Subscribe to get email updates!</p>
		<form action="http://donornetworkofarizona.createsend.com/t/i/s/tdthuu/" method="post" id="subForm">
			<input type="email" id="email" name="cm-tdthuu-tdthuu">
			<br>
			<input type="submit" value="Subscribe Now!">
		</form>
	</div>
	
	<div class="case-study">
		<?php // WP_Query arguments
		$args = array (
			'post_type'              => array( 'sidebar_stories' ),
			'posts_per_page'         => '1',
			'order'                  => 'DESC',
			'orderby'                => 'rand',
		);
		
		// The Query
		$query = new WP_Query( $args );
		?>
		
		<?php if($query->have_posts()): while($query->have_posts()): $query->the_post();?>
		<?php the_post_thumbnail('large');?>
		<h2><?php the_title();?></h2>
		
		<?php the_content();?>
		
		<a href="<?php the_field('click_url');?>" class="button button-green" target="_blank"><?php the_field('button_text');?></a>
		
		<?php endwhile; endif; wp_reset_postdata();?>
	</div>

</div>