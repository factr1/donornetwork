<?php
// WP_Query arguments
$args = array (
	'post_type'              => array( 'hospital_coordinator' ),
	'posts_per_page'         => '-1',
	'order'					=> 'ASC',
	'orderby'				=> 'title',
	
);

// The Query
$query = new WP_Query( $args );	

if($query->have_posts()):
?>
<select id="hospital_list">
	<option>
		-- Select Hospital --
	</option>
	<?php while($query->have_posts()): $query->the_post();?>
	<option value="<?php the_permalink();?>">
		<?php the_title();?>
	</option>
	<?php endwhile;?>
</select>
<?php endif; wp_reset_postdata();?>

<script>
	$(function(){
	  // bind change event to select
	  $('#hospital_list').bind('change', function () {
	    var url = $(this).val(); // get selected option value
	    if (url) { // require url to have value
	      window.location = url; // open url
	    }
	    return false;
	  });
	});
</script>