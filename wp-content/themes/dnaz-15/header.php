<!doctype html>

<!--[if lt IE 7 ]> <html lang="en" class="no-js ie ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie ie9"> <![endif]-->
<!--[if gt IE 9]>  <html lang="en" class="no-js ie">     <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js">    <!--<![endif]-->


<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" />
	<?php } ?>


<meta property="og:title" content="<?php the_title(); ?>" />
<meta property="og:site_name" content="<?php bloginfo('name') ?>">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>


<?php wp_head(); ?>

<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/normalize.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/foundation.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/f1-nav.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/slick.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/slick-theme.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/swipebox.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/outdatedbrowser.min.css">

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />

<script src="<?php bloginfo('template_url');?>/js/iframeResizer.min.js"></script>

<!--[if IE 8]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/modernizer.js"></script>
<![endif]-->



</head>

<body <?php body_class(); ?>>

<header>
	<div class="row header-container">
		<div class="small-12 text-center medium-3 columns">
			<h1>
				<a href="<?php bloginfo('url');?>/">
					Donor Network of Arizona
				</a>
			</h1>
			<a href="<?php bloginfo('url');?>/" class="logo">
				<img src="<?php bloginfo('template_url');?>/images/logo.png" alt="Donor Network of Arizona logo">
			</a>
		</div>
		<!-- Mobile 'Burger -->
		<div class="small-12 columns text-center show-for-small-only">
			<a id="nav-toggle" class="mm_open"><span></span></a>
		</div>
		<!-- Mobile Panel -->
		<div class="panel">
			<div class="row">
				<div class="small-12 text-center columns">
					<?php wp_nav_menu(array('theme_location' => 'primary'));?>

					<a href="https://register.donatelifeaz.org/register/?lang=en" class="button">Register Now</a>
				</div>
				<div class="small-12 text-center search-mobile">
					<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
						<div>
							<input type="text" value="" name="s" id="s" placeholder="Search" autocomplete="off" />
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="medium-5 columns">

			<div class="text-left social-icons">
				<?php if(get_field('facebook', 'option')){?>
				<a href="<?php the_field('facebook','option');?>" target="_blank">
					<i class="fa fa-facebook-square"></i>
				</a>
				<?php } if(get_field('twitter','option')){?>
				<a href="<?php the_field('twitter','option');?>" target="_blank">
					<i class="fa fa-twitter-square"></i>
				</a>
				<?php } if(get_field('instagram','option')){?>
				<a href="<?php the_field('instagram','option');?>" target="_blank">
					<i class="fa fa-instagram"></i>
				</a>
				<?php } if(get_field('youtube','option')){?>
				<a href="<?php the_field('youtube','option');?>" target="_blank">
					<i class="fa fa-youtube"></i>
				</a>
				<?php } if(get_field('pinterest','option')){?>
				<a href="<?php the_field('pinterest','option');?>" target="_blank">
					<i class="fa fa-pinterest-square"></i>
				</a>
				<?php } if(get_field('linkedin','option')){?>
				<a href="<?php the_field('linkedin','option');?>" target="_blank">
					<i class="fa fa-linkedin"></i>
				</a>
				<?php } if(get_field('snapchat','option')){?>
				<a href="<?php the_field('snapchat','option');?>" target="_blank">
					<i class="fa fa-snapchat-square"></i>
				</a>
				<?php } ?>

			</div>

			<div class="text-left utility">
				<?php wp_nav_menu(array('theme_location' => 'utility'));?>
			</div>


		</div>
		<div class="medium-4 columns">
			<div class="text-center register hide-for-small-only">
				<a href="https://register.donatelifeaz.org/register/?lang=en" class="button">Register Now</a>
			</div>
		</div>
	</div>

	<nav class="hide-for-small-only">
		<div class="row">
			<div class="medium-12 columns">
				<?php wp_nav_menu(array('theme_location' => 'primary'));?>
			</div>
		</div>
	</nav>

	<!-- Search Bar -->
	<div class="search-row">
		<div class="search-box row">
			<div class="small-6 columns right">
				<i class="fa fa-search"></i> <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			    <div><label class="screen-reader-text" for="s">Search for:</label>
			        <input type="text" value="" name="s" id="s" placeholder="Search" autocomplete="off" />
<!-- 			        <input type="submit" id="searchsubmit" value="Search" /> -->
			    </div>
			</form>
			</div>
		</div>
	</div>

<!-- Mega Menus -->
	<!-- About Us -->
	<div class="mega-menu about-us">
		<div class="row">
			<div class="medium-4 columns">
				<?php wp_nav_menu(array('theme_location' => 'about-us'));?>
			</div>
			<div class="medium-8 columns">
				<p>
					<?php the_field('about_us','option');?>
				</p>
			</div>
		</div>
	</div>

	<!-- Professional Partners -->
	<div class="mega-menu professional-partners">
		<div class="row">
			<div class="medium-4 columns">
				<?php wp_nav_menu(array('theme_location' => 'professional-partners'));?>
			</div>
			<div class="medium-8 columns">
				<p>
					<?php the_field('professional_partners','option');?>
				</p>
			</div>
		</div>
	</div>

	<!-- Understanding Donation -->
	<div class="mega-menu understanding-donation">
		<div class="row">
			<div class="medium-4 columns">
				<?php wp_nav_menu(array('theme_location' => 'understanding-donation'));?>
			</div>
			<div class="medium-8 columns">
				<p>
					<?php the_field('understanding','option');?>
				</p>
			</div>
		</div>
	</div>

	<!-- Donor Families -->
	<div class="mega-menu donor-families">
		<div class="row">
			<div class="medium-4 columns">
				<?php wp_nav_menu(array('theme_location' => 'donor-families'));?>
			</div>
			<div class="medium-8 columns">
				<p>
					<?php the_field('donor_families','option');?>
				</p>
			</div>
		</div>
	</div>

	<!-- Transplant Recipients -->
	<div class="mega-menu transplant-recipients">
		<div class="row">
			<div class="medium-4 columns">
				<?php wp_nav_menu(array('theme_location' => 'transplant-recipients'));?>
			</div>
			<div class="medium-8 columns">
				<p>
					<?php the_field('transplant_recipients','option');?>
				</p>
			</div>
		</div>
	</div>

	<!-- Get Involved -->
	<div class="mega-menu get-involved">
		<div class="row">
			<div class="medium-4 columns">
				<?php wp_nav_menu(array('theme_location' => 'get-involved'));?>
			</div>
			<div class="medium-8 columns">
				<p>
					<?php the_field('get_involved','option');?>
				</p>
			</div>
		</div>
	</div>

</header>
