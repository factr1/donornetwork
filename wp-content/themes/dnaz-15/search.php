<?php get_header(); ?>

<section class="page-header-no-image">
	<div class"row">
		<div class="medium-10 columns medium-centered text-center">
			<h1>
				Search Results
			</h1>
		</div>
	</div>
</section>

<section class="search-content">
	<div class="row">
		<div class="medium-8 columns">
							
			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
			
			<?php if (have_posts()) : ?>			
			<?php while (have_posts()) : the_post(); ?>
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>" style="margin-bottom: 60px;">
					<h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
					<?php //include (TEMPLATEPATH . '/inc/meta.php' ); ?>
							
					<?php the_excerpt(); ?>
					
					<a href="<?php the_permalink();?>" class="button button-green">Read More</a>
							
				</div>				
			<?php endwhile; ?>
			
			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>		
			
			<?php else : ?>
				<h2>No Results Found</h2>
			<?php endif;?>		
		</div>
		
		<div class="medium-4 columns">
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>