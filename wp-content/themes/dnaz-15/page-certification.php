<?php /* Template Name: Certification iFrame */
	get_header();
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0']; 
?>

	<!-- If we are showing an image header -->
	<?php if(get_field('show_header') == true):?>
		<section class="page-header" style="background: url(<?=$url?>) center center no-repeat;">
			<div class="slide-content row">
				<div class="medium-10 medium-centered content-container columns">
					<h1>
						<?php the_title();?>
					</h1>
					<?php if(get_field('page_description')): the_field('page_description'); endif;?>
				</div>
			</div>
		</section>
	<?php else:?>
		<section class="page-header-no-image">
			<div class"row">
				<div class="medium-10 columns medium-centered text-center">
					<h1>
						<?php the_title();?>
					</h1>
					<?php if(get_field('page_description')): the_field('page_description'); endif;?>
				</div>
			</div>
		</section>
	<?php endif;?>
	
	<div class="page-navigation">
		<div class="row">
			<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
		</div>
	</div>
	
	<section class="row page-content-container">
		<div class="medium-12 columns">
			<article>
				<iframe src="https://cert.dnaz.org/onlinecert" width="100%" height="100%" scrolling="no" class="iframe-content hide-for-small-only"></iframe>
				<script type="text/javascript">

					iFrameResize({
						log                     : false,                  // Enable console logging
						enablePublicMethods     : true,                  // Enable methods within iframe hosted page
						heightCalculationMethod : 'lowestElement',
						checkOrigin				: false,
						resizedCallback: function() {
                       
                           window.scrollTo(0,0)
                       
                       }
					});
		
		
				</script>
				
				<p class="show-for-small-only">
					The Designated Tissue Requestor Recertification module is not available on mobile. To access this section, please visit the site using a large tablet, laptop or desktop computer. 
				</p>
				
			</article>
		</div>

	</section>
	
<?php get_footer(); ?>
