<?php 
	get_header();
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0']; 
?>

<!-- If we are showing an image header -->
<?php if(get_field('show_header') == true):?>
	<section class="page-header hide-for-small-only" style="background: url(<?=$url?>) center top no-repeat;">
		<div class="slide-content row">
			<div class="medium-10 medium-centered content-container columns">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
	
	<!-- Mobile Hero w/ image -->
	<section class="page-header show-for-small-only">
		<img src="<?=$url?>" alt="Donor Network of Arizona image">
		<div class="content-container">
			<h1>
				<?php the_title();?>
			</h1>
			<?php if(get_field('page_description')): the_field('page_description'); endif;?>
		</div>
	</section>
	
<?php else:?>
	<section class="page-header-no-image">
		<div class"row">
			<div class="medium-10 columns medium-centered text-center">
				<h1>
					<?php the_title();?>
				</h1>
				<?php if(get_field('page_description')): the_field('page_description'); endif;?>
			</div>
		</div>
	</section>
<?php endif;?>

<div class="page-navigation">
	<div class="row">
		<?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?> 
	</div>
</div>

<section class="row page-content-container">
	<div class="medium-8 columns">
		<?php if(have_posts()): while(have_posts()): the_post();?>
			<article>
				<?php the_content();?>
			</article>
		<?php endwhile; endif;?>
	</div>
	<div class="medium-4 columns">
		<?php get_sidebar();?>
	</div>
</section>
	
<?php get_footer(); ?>
