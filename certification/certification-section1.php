
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>
</head>
<body>
<section>
	<div class="row">
		<div class="small-12 columns">
			<article>
				<h2>Objective One: Explain the background of eye and tissue donation, how the gist helps, and the recovery process.</h2>
				<h3>A: Background</h3>
				<p>Tissue and eye donation is a valuable gift that can restore sight and mobility, and improve the quality of life for many people suffering from painful and debilitating bone and connective tissue diseases. All major religions support organ and tissue donation as well as an overwhelming majority of Americans. A single eye and tissue donation can help more than 100 people. People in America receive more than 1.5 million tissue transplants annually.</p>
				<p>The following <strong>tissue</strong> can be donated:</p>
				<ul>
					<li>Eyes</li>
					<li>Bone and connective tissue</li>
					<li>Skin tissue</li>
					<li>Heart valves</li>
					<li>Pericardium</li>
					<li>Saphenous and femoral veins</li>
				</ul>
				<p>
					Following is key information regarding tissue and eye donation about which healthcare professionals need to be aware. Please note the descriptions are in lay terms to provide suggestions on how you may share the information with a family. <a href="#" target="_blank">Tissue Fact Sheet</a>
				</p>
				<h3>B: How the Gift Helps</h3>
				<p>
					Eye donation can restore the sight of people through cornea transplantation or eye tissue research. One cornea donor can restore sight for two people.
				</p>
				<p>
					Bone is used in a variety of procedures including reconstructive surgery, spinal stabilization, orthopedic procedures and in limb-saving surgeries for patients afflicted with severe fractures, bone trauma or tumors.
				</p>
				<p>
					Other tissues that can be donated include tendons and ligaments (often used to repair sports or stress-related injuries), leg vessels for femoral bypass procedures, and skin tissue which is used as a temporary dressing for burn victims or to make collagen for reconstructive surgeries such as vocal
cord reconstruction). Heart valves can be utilized to repair cardiac defects in children and adults. Approximately 60%-70% of all heart valves donated go to children.
				</p>
				<h3>C: Recovery</h3>
				<p>
					For tissue donation, the donor may be transported to a different facility for recovery. Tissue and eye recovery is a specialized procedure that will not prevent an open-casket funeral service. If the death comes under the jurisdiction of the Medical Examiner, they will determine if an autopsy is necessary and if there are any restrictions regarding the donation process.
				</p>
				<p>
					Optimal time for ocular recovery is within eight hours of death. Eye donation may involve the recovery of the whole eye. The eyes/corneas are surgically recovered by specially trained technicians. Minor swelling, bleeding and or bruising, although rare, can occur as with any surgical procedure. Eye tissue and sclera can be used for research and education (if authorization for research is granted). Funeral directors will place a form in the shape of an eye, and carefully close the lids to allow for an open- casket funeral service.
				</p>
				<p>
					Bone and connective tissue (such as ligaments and tendons) are recovered from the legs, back, pelvis and upper arms. After the recovery, the bone is replaced with an appropriately shaped form, and the area is surgically closed. A surgical incision remains and can usually be covered by clothing.
				</p>
				<p>
					The entire heart is recovered in order to more carefully remove the valves, which are then prepared for transplantation. The donation will be coordinated by the Hospital, DNA and the funeral home so it does not interfere with funeral plans.
				</p>
			</article>
		</div>
	</div>
	<div class="row">
		<form role="form" method="POST" action="http://www.donornetwork.dev/certification/section/1">
		<input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
			<div class="row">
				<div class="small-12 columns">
					<h2>Section 1 Quiz: Answer the questions correctly to go to the next section</h2>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<div>1. A tissue donor can potentially help more than _____ people.</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[cuducdscfnuucffsnftvnrfbfvuqqdsf]" value="upunuodrbcderuussdwcrtbuosfcrcfn"/>
						</div>
						<div class="small-11 columns">
							a. 10
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[cuducdscfnuucffsnftvnrfbfvuqqdsf]" value="tsbvoqqnqnabcaevnawqnqpdtaqvwbub"/>
						</div>
						<div class="small-11 columns">
							b. 25
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[cuducdscfnuucffsnftvnrfbfvuqqdsf]" value="pcuwcvweowcvappbtfetffsduscvaeed"/>
						</div>
						<div class="small-11 columns">
							c. 50
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[cuducdscfnuucffsnftvnrfbfvuqqdsf]" value="ddfecbcunuvtenvvffpcvuwvewtfbcwt"/>
						</div>
						<div class="small-11 columns">
							d. 100
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<div>2. The following tissues can be recovered from a deceased donor:</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[noeppdpweuobrbwrdudpnrwqofqooobv]" value="cptaoufnrtupefttvqwawfaoqectrfro"/>
						</div>
						<div class="small-11 columns">
							a. eyes and bone only
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[noeppdpweuobrbwrdudpnrwqofqooobv]" value="bwstvndwufappvobedscpwqsuousrqto"/>
						</div>
						<div class="small-11 columns">
							b. bone, soft tissue (tendons/ligaments), veins, skin, and heart valves
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[noeppdpweuobrbwrdudpnrwqofqooobv]" value="nocostawavwfetedapqoccdrsvsrrtbf"/>
						</div>
						<div class="small-11 columns">
							c. bone, soft tissue (tendons/ligaments), veins, skin, heart valves and eyes
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[noeppdpweuobrbwrdudpnrwqofqooobv]" value="taqbdwvuaoutcspnccaaapdntavbqvau"/>
						</div>
						<div class="small-11 columns">
							d. liver, eyes, bone, soft tissue (tendons/ligaments), skin, veins, heart valves
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<div>3. Eye recovery is accomplished when the eye tissue is removed, a form is placed,  and the eyelids are gently closed so that an open casket service is still possible.</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[bfbuuuwowwuvbsnfuacsuftooavpapnw]" value="etqodwoqdpbtrqanvnrqevtutfowcnpw"/>
						</div>
						<div class="small-11 columns">
							TRUE
						</div>
					</div>
				<div class="row">
					<div class="small-1 columns text-right">
						<input type="radio" name="responses[bfbuuuwowwuvbsnfuacsuftooavpapnw]" value="ucfwbfbdfdvqvaapnuofnpvrrsvpbrfw"/>
					</div>
					<div class="small-11 columns">
						FALSE
					</div>
				</div>                
			</div>
			</div> 
			<div class="row">
				<div class="small-12 columns">
					<div>4. The optimal time for eye recovery is within eight hours of death.</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[foufpseetbsvsqbeuntrcfuqubsrbpqs]" value="pocubtsdonfberadoqsopwpoduwautcw"/>
						</div>
						<div class="small-11 columns">
							TRUE
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[foufpseetbsvsqbeuntrcfuqubsrbpqs]" value="orfvvewrfcvoencustnffoswdeqobrsf"/>
						</div>
						<div class="small-11 columns">
							FALSE
						</div>
					</div>                
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<div>5. All major religions support organ and tissue donation.</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[vropqtuanctpsenruwvvronodbfewrup]" value="reudasbnapqtuctbostnwdvdcppbsdse"/>
						</div>
						<div class="small-11 columns">
							TRUE
					</div>
				</div>
				<div class="row">
					<div class="small-1 columns text-right">
						<input type="radio" name="responses[vropqtuanctpsenruwvvronodbfewrup]" value="dwadsnbdwbrapdwcaqqwsrnsoupqaqrp"/>
					</div>
					<div class="small-11 columns">
						FALSE
					</div>
				</div>                
			</div>
			</div> 
			<div class="row">
				<div class="small-12 columns">
					<div>6. Medical Examiner cases cannot be donors.</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[aceesfcwucubeudanuspoosdndsoabuc]" value="nftufsutssdsvppuwudwfpwdqncuerqu"/>
						</div>
						<div class="small-11 columns">
							TRUE
						</div>
					</div>
					<div class="row">
						<div class="small-1 columns text-right">
							<input type="radio" name="responses[aceesfcwucubeudanuspoosdndsoabuc]" value="fpprddfwndsfbqsfuaoqdnfobefwceds"/>
						</div>
						<div class="small-11 columns">
							FALSE
						</div>
					</div>              
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<p>For further information see: <a href="#" target="_blank">Tissue Fact Sheet</a></p>
				</div>
			</div>
			
			<div class="row messages"> 
			   <div class="small-12 columns"> 
			   <div data-alert class="alert-box alert radius"> 
			      <h1>Whoops!</h1> 
			     <ul class="no-bullet"> 
			      <li>Question #1 has not been answered.</li> 
			      <li>Question #2 has not been answered.</li> 
			      <li>Question #3 has not been answered.</li> 
			      <li>Question #4 has not been answered.</li>
			      <li>Question #5 has not been answered.</li> 
			      <li>Question #6 has not been answered.</li> 
			     </ul> 
			    <a href="#" class="close">&times;</a> 
			   </div> 
			</div>
			
			<div class="row">
				<div class="small-12 columns">
					<a href="#" class="button small radius submit">Continue &gt; &gt;</a>
				</div>
			</div>
		</form>
	</div>
</section>
</body>
</html>