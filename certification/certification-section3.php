
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>
</head>
<body>
	<section>
		<div class="row">
			<div class="small-12 columns">
				<article>
					<h2>Objective Three: Differentiate between a potential organ and tissue donor and the referral process for each.</h2>
					<h3>A: Potential organ and tissue donors</h3>
					<p>
						Though organ donation requires the special circumstances of a severe neurological injury, eye and tissue donation occurs after the cessation of cardiopulmonary function (cardiac death). Donation will only proceed after the patient is declared legally dead. (Brain death or cardiac death)
					</p>
					<p>
						The law requires that you call DNA with every death/imminent death. DNA will determine if there is potential for donation. There will be no discussion of donation with the family if it is not deemed an option. Patients who sustain cardiac death can potentially be eye and/or tissue donors.
					</p>
					<h3>B: The Referral Process</h3>
					<p class="text-center"><strong>Call BEFORE discussing donation with the family</strong></p>
					<ol>
						<li>Call Donor Network of Arizona (1-800-447-9477) with every death within ONE hour of:
							<ol>
								<li>Cardiac Death</li>
								<li>When death is "imminent" (Clinical Triggers are met)</li>
								<li>When DNR/AND (Do Not Resuscitate/Allow Natural Death) or Withdrawal is being considered</li>
							</ol>
						<li>DNA evaluates the patient to determine if transplantation is possible. DNA verifies patient’s status on the Donor Registry.</li>
						<li>The <a href="#" target="_blank">Anatomical Gift Notification Form</a> is completed for every patient death.</li>
						<li>The Designated Tissue Requestor will discuss eye and tissue donation with the family/legal decision-maker or DNA may do this via telephone.</li>
						<li>Complete the Arizona Anatomical Gift Authorization form, call DNA to notify of the family’s decision and receive fax number, fax paperwork and file in the patient’s medical record.</li>
					</ol>
					<p>For more information about the referral process: <a href="#" target="_blank">Click Here</a></p>
				</article>
			</div>
		</div>
	</section>
        <section>
    <div class="row">
                <form role="form" method="POST" action="http://www.donornetwork.dev/certification/section/3">
            <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
            <div class="row">
                <div class="small-12 columns">
                    <h2>Section 3 Quiz: Answer the questions correctly to go to the next section</h2>
                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>9. The DNA telephone number to make a referral is 1-800-447-9477.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[prbburnasuffsfcuqwtfeuvtettnqcee]" value="oosdopuwapnsantddaeuesrdrqecfnbe"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[prbburnasuffsfcuqwtfeuvtettnqcee]" value="bbaevwqoqddnrqcdtsnfdvrobpuednuv"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>10. If a patient expires in your unit, the first thing to do regarding donation is:</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[rppbnafpceetopqdwvdnnbuvuetsuuaw]" value="rqofqodqaqsfrdvrnnfptbwaadntnrqb"
         />
    </div>
    <div class="small-11 columns">
        a. discuss donation with the legal decision-maker
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[rppbnafpceetopqdwvdnnbuvuetsuuaw]" value="acsqwpbpfftdapetssduuaqonpsdpffp"
         />
    </div>
    <div class="small-11 columns">
        b. call Donor Network of Arizona to determine donor potential
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[rppbnafpceetopqdwvdnnbuvuetsuuaw]" value="dsodtertrdcetdfaetpsbbddtnveqfrs"
         />
    </div>
    <div class="small-11 columns">
        c. check with the physician to make sure it is okay to pursue donation
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[rppbnafpceetopqdwvdnnbuvuetsuuaw]" value="ofrobvbbnbsvprtqvpvswfutfasovvsb"
         />
    </div>
    <div class="small-11 columns">
        d. determine who the legal decision-maker is
    </div>
</div>
                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>11. The best time to call Donor Network of Arizona referral line to report a death is:</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[qdrwanrocvendtdpeoaandnsadvfvsub]" value="upnfcooooftottuwfvqvfpqffpvvqtup"
         />
    </div>
    <div class="small-11 columns">
        a. after discussing donation options with the family
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[qdrwanrocvendtdpeoaandnsadvfvsub]" value="vueqaofoqovvebrvffbwaccrsqvwcauw"
         />
    </div>
    <div class="small-11 columns">
        b. within 1 hour of cardiac death
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[qdrwanrocvendtdpeoaandnsadvfvsub]" value="pfrovtrwacbwbbesondqtccnwvvprpvb"
         />
    </div>
    <div class="small-11 columns">
        c. prior to the family discussion about eye and tissue donation
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[qdrwanrocvendtdpeoaandnsadvfvsub]" value="vncrntqssdtnvwapqwqqpnuqswsdrfww"
         />
    </div>
    <div class="small-11 columns">
        d. both b and c
    </div>
</div>
                </div>
            </div>
                         <div class="row">
                <div class="small-12 columns">
                    <a href="#" class="button small radius submit">Continue &gt; &gt;</a>
                </div>
            </div>
        </form>
    </div>
</section></body>
</html>