
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>

</head>
<body>
	<section class="row">
		<div class="small-12 columns">
			<article>
				<h2>Objective Six: Initiate and complete the Anatomical Gift Authorization Form.</h2>
				<p>
					The form is available at most hospitals – find out where your form is kept. Use the Spanish translated form when appropriate. The form documents the tissues authorized for donation; the identified family/legal decision-maker and the signatures for authorization (legal decision-maker, Designated Tissue Requestor, a witness (optional) and translator (required) if used). You must provide any family authorizing donation with accurate information so that they are giving “Informed Consent.” Telephone conversations must be recorded and will be performed by DNA.
				</p>
				<h3 class="text-center">
					Elements of Informed Consent<br>
as explained on the Arizona Anatomical Gift Authorization Form
				</h3>
				<ul>
					<li>
						Specify the tissues that can be donated
					</li>
					<li>
						Describe the recovery process, including:
						<ul>
							<li>
								Timing
							</li>
							<li>
								Advise that for recovery of eye and tissue in Phoenix, the body may be transferred to a
donation facility for recovery procedures
							</li>
						</ul>
					</li>
					<li>
						Validation of death
					</li>
					<li>
						No guarantee of recovery and/or transplantation
					</li>
					<li>
						Blood tests done for communicable diseases
					</li>
					<li>
						Further testing will be done to determine suitability for transplantation
					</li>
					<li>
						Legal decision-maker authorizes tissues for recovery on the form
					</li>
					<li>
						Explain the family’s financial obligation concerning hospital and funeral expenses
					</li>
					<li>
						If necessary, explain how the Medical Examiner will be involved
					</li>
					<li>
						How will donation affect funeral plans
					</li>
					<li>
						Explain the differences between nonprofit and for-profit organizations
					</li>
					<li>
						The distribution of corneas and tissue
					</li>
				</ul>
				<p>To view the Legal Decision-Maker hierarchy: <a href="#">click here</a>.</p>
			</article>
		</div>
	</section>
        <section>
    <div class="row">
                <form role="form" method="POST" action="http://www.donornetwork.dev/certification/section/6">
            <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
            <div class="row">
                <div class="small-12 columns">
                    <h2>Section 6 Quiz: Answer the questions correctly to go to the next section</h2>
                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>23. The Domestic Partner out ranks Parent in the hierarchy of Legal next-of-kin.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[wowcuwfovscvucctcuurrnrvpbctqoqn]" value="rsvvafrnawwberaoounffsaradtsnevn"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[wowcuwfovscvucctcuurrnrvpbctqoqn]" value="cvrrnevdbvsuetcufrnentroqqnpuoqp"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>24. The legal decision-maker must sign the form and anyone else can be the witness.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[vaqqturnadbopnnfrcpfupsasfucqbbs]" value="pudnfbcqbqfvuwdnpntpnwpaqncvqduv"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[vaqqturnadbopnnfrcpfupsasfucqbbs]" value="brnonstwsurtfrubewpssfptnawonqen"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>25. If a translator is used, they do not need to sign the form.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ecpbrveevtnrossqqfvtuwcoevutdwuo]" value="dqpdnasnbonftbreqrrtdapnanbaooon"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ecpbrveevtnrossqqfvtuwcoevutdwuo]" value="varcnwddrtofuuaaupcdfacwspdwvucq"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                         <div class="row">
                <div class="small-12 columns">
                    <a href="#" class="button small radius submit">Continue &gt; &gt;</a>
                </div>
            </div>
        </form>
    </div>
</section></body>
</html>