
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>
</head>
<body>
	<section class="row">
		<div class="small-12 columns">
			<p><strong>To complete the course, fill out the evaluation form, download and print the certificate of completion and the Designated Tissue Requestor pledge.</strong></p>
			<p>
				For additional information on classes/education, contact: Lisa Kula<br>
				<a href="mailto:Lisa.Kula@dnaz.org">Lisa.Kula@dnaz.org</a><br>
				(602) 222-3813<br>
				Donor Network of Arizona 201 W. Coolidge Phoenix, AZ 85013<br>
			</p>
		</div>
	</section>
        <section>
    <div class="row">
                <form role="form" method="POST" action="http://www.donornetwork.dev/certification/survey">
            <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
            <div class="row">
                <div class="small-12 columns">
                    <h2>Course Evaluation: Answer the following questions to go to the next section</h2>
                    <p>Please complete the survey with 1 being strongly disagree and 5 being strongly agree. Your feedback is appreciated!</p>
                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>1. This class was convenient to take.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[aqufoandvqwcafbudrcqrrdarqotwnnr]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[aqufoandvqwcafbudrcqrrdarqotwnnr]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[aqufoandvqwcafbudrcqrrdarqotwnnr]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[aqufoandvqwcafbudrcqrrdarqotwnnr]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[aqufoandvqwcafbudrcqrrdarqotwnnr]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>2. The information was valuable and will help me with family discussions.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[tooqtpqqbfqbpcecqocucaqopnebveup]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[tooqtpqqbfqbpcecqocucaqopnebveup]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[tooqtpqqbfqbpcecqocucaqopnebveup]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[tooqtpqqbfqbpcecqocucaqopnebveup]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[tooqtpqqbfqbpcecqocucaqopnebveup]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>3. This class answered questions I had about family discussions.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[osnneqrqvdfdaeeuunvwaceevvdpdddc]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[osnneqrqvdfdaeeuunvwaceevvdpdddc]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[osnneqrqvdfdaeeuunvwaceevvdpdddc]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[osnneqrqvdfdaeeuunvwaceevvdpdddc]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[osnneqrqvdfdaeeuunvwaceevvdpdddc]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>4. I would have preferred training in a classroom with a trained facilitator.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[ebnvrnvsecrpbfbeddvtavstppstporo]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[ebnvrnvsecrpbfbeddvtavstppstporo]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[ebnvrnvsecrpbfbeddvtavstppstporo]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[ebnvrnvsecrpbfbeddvtavstppstporo]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[ebnvrnvsecrpbfbeddvtavstppstporo]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
            
            
            <div class="row">
				<div class="small-12 columns">
					<div>5. As a result of this class, please share at least one action you will take to change your professional practice/performance:</div>
					<div class="row">
						<div class="small-12 columns">
							<textarea name="responses[facottcpusqnqsnecewoorsvecrowdtc]"></textarea>
						</div>
					</div>                
				</div>
			</div>
            
                        <div class="row">
                <div class="small-12 columns">
                    <div>
	                 <h3>After completing this course, how prepared are you to:</h3>
	                 6. Explain the background of eye and tissue donation, how the gift helps, and the recovery process.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[saeocqpstoedsdndqqwquvwwowoanaen]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[saeocqpstoedsdndqqwquvwwowoanaen]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[saeocqpstoedsdndqqwquvwwowoanaen]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[saeocqpstoedsdndqqwquvwwowoanaen]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[saeocqpstoedsdndqqwquvwwowoanaen]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>7. Identify key legislation and hospital compliance requirements regarding the donation process, including the Donor Registry.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[pqurpuuserpvwvosfpvdsdnbnsbeuufu]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[pqurpuuserpvwvosfpvdsdnbnsbeuufu]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[pqurpuuserpvwvosfpvdsdnbnsbeuufu]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[pqurpuuserpvwvosfpvdsdnbnsbeuufu]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[pqurpuuserpvwvosfpvdsdnbnsbeuufu]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>8. Differentiate between a potential organ and tissue donor and the referral process for each.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[oseofqwbuppaupvvcscwuvacatwpwrdd]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[oseofqwbuppaupvvcscwuvacatwpwrdd]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[oseofqwbuppaupvvcscwuvacatwpwrdd]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[oseofqwbuppaupvvcscwuvacatwpwrdd]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[oseofqwbuppaupvvcscwuvacatwpwrdd]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>9. Identify your supportive role in the family discussion for eye and tissue donation.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[auwncafeuuoapbannpufowbpadvrruce]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[auwncafeuuoapbannpufowbpadvrruce]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[auwncafeuuoapbannpufowbpadvrruce]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[auwncafeuuoapbannpufowbpadvrruce]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[auwncafeuuoapbannpufowbpadvrruce]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>10. Perform individualized, culturally appropriate donation discussions with families regarding eye and tissue donation.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[qvucsotvnopawqoudbpcrwvdvvqaqvof]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[qvucsotvnopawqoudbpcrwvdvvqaqvof]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[qvucsotvnopawqoudbpcrwvdvvqaqvof]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[qvucsotvnopawqoudbpcrwvdvvqaqvof]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[qvucsotvnopawqoudbpcrwvdvvqaqvof]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>11. Initiate and complete the Anatomical Gift Authorization Form.</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[qrquoqpeuetvfooadquaqbeaetautars]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[qrquoqpeuetvfooadquaqbeaetautars]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[qrquoqpeuetvfooadquaqbeaetautars]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[qrquoqpeuetvfooadquaqbeaetautars]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[qrquoqpeuetvfooadquaqbeaetautars]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>12. I would recommend this class to peers:</div>
                    <div class="row">
    <div class="small-12 columns">
                 <input type="radio" name="responses[powwatnsobdvunvooardwarpewqctbru]" value="1"
         /> 1&nbsp;&nbsp;
                 <input type="radio" name="responses[powwatnsobdvunvooardwarpewqctbru]" value="2"
         /> 2&nbsp;&nbsp;
                 <input type="radio" name="responses[powwatnsobdvunvooardwarpewqctbru]" value="3"
         /> 3&nbsp;&nbsp;
                 <input type="radio" name="responses[powwatnsobdvunvooardwarpewqctbru]" value="4"
         /> 4&nbsp;&nbsp;
                 <input type="radio" name="responses[powwatnsobdvunvooardwarpewqctbru]" value="5"
         /> 5&nbsp;&nbsp;
            </div>
</div>                </div>
            </div>
                        
                        <div class="row">
                <div class="small-12 columns">
                    <div>13. The most valuable part of this class:</div>
                    <div class="row">
    <div class="small-12 columns">
        <textarea name="responses[stnoeupffvtcefwsbbspbnvspbbrbtct]"></textarea>
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>14. The class could be improved by:</div>
                    <div class="row">
    <div class="small-12 columns">
        <textarea name="responses[eeacqqvasvdvpcquvocavwfpvrrwfqsp]"></textarea>
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>15. Additional donation topics I would like information on:</div>
                    <div class="row">
    <div class="small-12 columns">
        <textarea name="responses[vtuprvedtdaaeapsvrrwduaeccooedba]"></textarea>
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>16. Additional comments:</div>
                    <div class="row">
    <div class="small-12 columns">
        <textarea name="responses[wvcnqodpovousbfectscpceuanrbwwws]"></textarea>
    </div>
</div>                </div>
            </div>
                         <div class="row">
                <div class="small-12 columns survey-submit">
                    <a href="#" class="button small radius submit">Continue &gt; &gt;</a>
                </div>
            </div>
        </form>
    </div>
</section></body>
</html>