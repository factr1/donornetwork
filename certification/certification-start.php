<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>
</head>
<body>
	<section class="row">
		<div class="small-12 text-center columns mission-statement">
			<h1>Mission</h1>
			<h2>We make the most of life through the gift of organ and tissue donation.</h2>
			<br>
			<h1>Vision</h1>
			<h2>We challenge ourselves and others every day to realize Arizona's potential to save and improve lives.</h2>
			<hr>
		</div>
		<div class="small-12 columns">
			<p><strong>An Important Reminder:</strong></p>
			<p>
				As you review the material in this study guide, you will receive valuable information on discussing eye and tissue donation with the patient’s family. As you gain experience and become more comfortable conducting the donation discussion, please remember that the staff of Donor Network of Arizona (DNA) is always available to help you. Keep in mind that you may call Donor Network of Arizona (DNA) for assistance at 1-800-447-9477.
			</p>
			<p>
				<strong>Instructions:</strong><br>
				Please read through the material and answer the questions for each section.
			</p>
			
			<p class="legal">
			<strong>Approval Statement:</strong><br>
			The Donor Network of Arizona is an approved provider of continuing nursing education by the Western Multi-State Division, an accredited approver by the American Nurses Credentialing Center’s Commission on Accreditation.
		</p>
		<p class="legal">
			<strong>Disclaimer Statements:</strong><br>
			<u>In compliance with the guidelines for approved Contact Hour activities the following statements are offered for your review:</u><br>
			There are no financial conflicts of interest from the presenters for this approved course.
		</p>
		<p class="legal">
			All individuals in positions to control content of the educational activity have disclosed all financial relationships and there are no conflicts of interest.
		</p>
		<p class="legal">
			There is no commercial support of this educational activity.
		</p>
		<p class="legal">
			AzNA and the ANCC Commission on Accreditation do not approve or endorse any commercial products displayed.
		</p>
		<p class="legal">
			There is no off-label usage/no product related to this activity.
		</p>
			
			<p><strong>Glossary of terms:</strong></p>
			
			<div class="row text-left">
				<div class="small-2 columns">
					AAGAF
				</div>
				<div class="small-10 columns">
					Arizona Anatomical Gift Authorization Form
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					AGNF
				</div>
				<div class="small-10 columns">
					Anatomical Gift Notification Form
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					AOPO
				</div>
				<div class="small-10 columns">
					Association of Organ Procurement Organizations
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					ARAGA
				</div>
				<div class="small-10 columns">
					Arizona Revised Anatomical Gift Act
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					ARS
				</div>
				<div class="small-10 columns">
					Arizona Revised Statute
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					BD
				</div>
				<div class="small-10 columns">
					Brain Death
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					CMS
				</div>
				<div class="small-10 columns">
					Centers for Medicare and Medicaid Services
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					DCD
				</div>
				<div class="small-10 columns">
					Donation after Circulatory Death
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					DPC
				</div>
				<div class="small-10 columns">
					Donor Program Coordinator
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					DNA
				</div>
				<div class="small-10 columns">
					Donor Network of Arizona
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					DTR
				</div>
				<div class="small-10 columns">
					Designated Tissue Requestor
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					GCS
				</div>
				<div class="small-10 columns">
					Glasgow Coma Scale
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					HCFA
				</div>
				<div class="small-10 columns">
					Health Care Financing Administration (now CMS)
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					HHS
				</div>
				<div class="small-10 columns">
					Health and Human Services
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					IHC
				</div>
				<div class="small-10 columns">
					In-house Coordinator
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					JCAHO
				</div>
				<div class="small-10 columns">
					Joint Commission on Accreditation of Healthcare Organizations
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					ME
				</div>
				<div class="small-10 columns">
					Medical Examiner
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					OPO
				</div>
				<div class="small-10 columns">
					Organ Procurement Organization
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					ORC
				</div>
				<div class="small-10 columns">
					Organ Recovery Coordinator
				</div>
			</div>
			<div class="row text-left">
				<div class="small-2 columns">
					UNOS
				</div>
				<div class="small-10 columns">
					United Network of Organ Sharing
				</div>
			</div>
			
		</div>
	</section>
	
	<div class="row button-row">
		<div class="small-12 text-right">
        	<a href="http://www.donornetwork.dev/certification/section/1" class="button small right radius">Begin Test &gt; &gt;</a>
		</div>
	</div>
</body>
</html>