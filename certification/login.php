
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="/css/app.min.css" />
    <script src="/js/app.min.js" type="text/javascript"></script>
</head>
<body>
    <section class="auth-form">
        <div class="row">
        <form role="form" method="POST" action="http://www.donornetwork.dev/login">
    <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
    <fieldset>
        <legend>Login Form</legend>
        <div class="row">
            <div class="small-12 columns">
                <label>
                    <input type="email" name="email" value="" placeholder="Email Address" required />
                </label>
                <label>
                    <input type="password" name="password" value="" placeholder="Password" required />
                </label>
                <input type="checkbox" name="remember" /><label for="remember">Remember Me</label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <a href="#" class="button small radius submit">Login</a>
                <a href="http://www.donornetwork.dev/password/email">Forgot Your Password?</a>
            </div>
        </div>
    </feildset>
</form>
    </div>
</section>
</body>
</html>