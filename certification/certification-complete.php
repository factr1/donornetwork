<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>
</head>
<body>
	<section class="row">
		<div class="small-12 small-centered columns course-complete">
	        <h1>Congratulations!</h1>
			<a href="#" class="button small radius">Print Certificate</a>
			<br /><br />
			<a href="http://www.donornetwork.dev/certification" class="button small radius">Back to Start &gt; &gt;</a>
		</div>
	</section>
</body>
</html>