
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>
</head>
<body>
	<section class="row">
		<div class="small-12 columns">
			<h2>Objective Four: Identify your supportive role in the family discussion for eye and tissue donation.</h2>
			<h3>Tissue and Eye Donation: Roles and Responsibilities</h3>
			<p>The donation process is a collaborative effort between your hospital and DNA. The roles and responsibilities regarding tissue donation are as follows:</p>
			<p class="text-center"><u>Tissue Donation</u></p>
			<table class="tissue-donation">
				<thead>
					<tr>
						<td class="text-center">
							Hospital
						</td>
						<td class="text-center">
							Designated Requestor
						</td>
						<td class="text-center">
							DNA
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							Identifies referral
						</td>
						<td>
							Determines the legal decision maker
						</td>
						<td>
							Determines medical suitability for donation
						</td>
					</tr>
					<tr>
						<td>
							Calls the referral to DNA within ONE hour of every cardiac death
						</td>
						<td>
							Initiates a conversation with the family or defers the responsibility to DNA
						</td>
						<td>
							Verifies status on Arizona Donor Registry
						</td>
					</tr>
					<tr>
						<td>
							Locates a Designated Tissue Requestor or consults DNA to speak with family about donation
						</td>
						<td>
							Completes all paperwork accurately
						</td>
						<td>
							Serves as a resource to the hospital’s Designated Tissue Requestor
						</td>
					</tr>
					<tr>
						<td>
							Ensures that all hospital staff have access to the list of Designated Tissue Requestors
						</td>
						<td>
							&nbsp;
						</td>
						<td>
							Used as an alternative to hospital’s Designated Tissue Requestor
						</td>
					</tr>
					<tr>
						<td>
							Provides access to and a copy of the patient’s chart for DNA staff. This is allowable under HIPAA
						</td>
						<td>
							&nbsp;
						</td>
						<td>
							Collaborates with hospital staff on family discussions as needed
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
        <section>
    <div class="row">
                <form role="form" method="POST" action="http://www.donornetwork.dev/certification/section/4">
            <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
            <div class="row">
                <div class="small-12 columns">
                    <h2>Section 4 Quiz: Answer the questions correctly to go to the next section</h2>
                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>12. The role of the Hospital is to have staff call Donor Network of Arizona within ONE hour of a patient reaching cardiac death.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[aoefdruqbuuuaspnebfusodubonfutuw]" value="rqofeonssboepeftwcvdowpwtsppwqoo"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[aoefdruqbuuuaspnebfusodubonfutuw]" value="tqunreuvarqvbrocwabbunprwodeapdn"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>13. The role of Donor Network of Arizona staff is to gather information to determine suitability for donation.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[fcaptvarunnnbuwvcqtqcqevdutouovc]" value="enttctfcbbptdouqvdnfdapwtoeduect"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[fcaptvarunnnbuwvcqtqcqevdutouovc]" value="tppprpbwoevtraundqqrwnqwfrbsesen"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>14. The role of Donor Network of Arizona staff is to collaborate with hospital staff to plan the donation discussion.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ddnqtsropnqctqdnttreuqerqwnnbdqs]" value="ntwnewsborpwoecvdosonvbbcsobaqss"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ddnqtsropnqctqdnttreuqerqwnnbdqs]" value="fdvavqpcdcvenbtsnuptbffsoodoaebn"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>15. The role of the Designated Tissue Requestor is to offer the option of donation to families when there is a potential for eye and/or tissue donation.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[vdroffcvrdbbfqwdpobucsedbdwsbdwq]" value="quwvawvodavofbdcpstvdcvftsqrcsou"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[vdroffcvrdbbfqwdpobucsedbdwsbdwq]" value="ervfuuuwrueuouuufsnrabcwcbtwwrcd"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                         <div class="row">
                <div class="small-12 columns">
                    <a href="#" class="button small radius submit">Continue &gt; &gt;</a>
                </div>
            </div>
        </form>
    </div>
</section></body>
</html>