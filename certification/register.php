
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="/css/app.min.css" />
    <script src="/js/app.min.js" type="text/javascript"></script>
</head>
<body>
    <section class="auth-form">

    <div class="row">
        <form role="form" method="POST" action="http://www.donornetwork.dev/register">
    <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
    <fieldset>
        <legend>Registration Form</legend>
        <div class="row">
            <div class="small-12 columns">
                <label>First Name
                    <input type="text" name="first_name" value="" placeholder="name" required />
                </label>
                <label>Middle Initial
                    <input type="text" name="middle_name" value="" placeholder="initial" />
                </label>
                <label>Last Name
                    <input type="text" name="last_name" value="" placeholder="name" required />
                </label>
                <label>Email
                    <input type="email" name="email" value="" placeholder="email" required />
                </label>
                <label>Password
                    <input type="password" name="password" value="" placeholder="8-255 characters" required />
                </label>
                <label>Confirm Password
                    <input type="password" name="password_confirmation" value="" placeholder="password" required />
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label>Hospital
                    <select name="hospital_id">
                        <option value="">Select Hospital...</option>
                                                <option value="1" selected="selected" >Employee</option>
                                            </select>
                </label>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="small-12 columns">
                <label>Unit
                    <input type="text" name="unit" value="" placeholder="hospital unit" />
                </label>
                <label>RN Number
                    <input type="text" name="rn_number" value="" placeholder="RN number" />
                </label>
                <label>
                    <input type="checkbox" name="optin_donation_updates" value="1" class="inline" /> I would be interested in receiving informational updates on donation.
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <a href="#" class="button small radius submit">Register</a>
            </div>
        </div>
    </feildset>
</form>    </div>
</section>
</body>
</html>