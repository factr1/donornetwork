
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>

</head>
<body>
	<section class="row">
		<div class="small-12 columns">
			<article>
			<h2>Objective Five: Recognize how to perform individualized, culturally appropriate donation discussions with families regarding eye and tissue donation.</h2>
			<p>
				This may be a devastating time for families, but research shows families benefit from making the decision to donate. You provide an important support role for the family. You can help the family to reframe the situation, and focus on the patient’s beliefs and values, rather than his or her death.
			</p>
			<h3>
				<strong>The Family Centered and Value-Focused Conversation</strong>
			</h3>
			<p>
				When asked in their best moment, an overwhelming majority of Americans said that they would indeed save or enhance a life through the gift of organ and tissue donation. The decision to donate will be addressed by a Designated Tissue Requestor at a time when families are at their very worst moment; a raw and vulnerable moment.
			</p>
			<p>
				A Designated Tissue Requestor’s role is to have a family centered and value focused conversation by:
			</p>
			<ul>
				<li>Allowing the family time for reflection and discussion. This is an important part of the
bereavement process, regardless of the family’s final decision about donation.</li>
				<li>Sensitively and appropriately discussing how their loved one can enhance and improve the
lives of others, not performing a monologue of facts and information.</li>
			</ul>
			<h3>
				<strong>Response vs. Reaction</strong>
			</h3>
			<p>
				When the conversation about donation begins there may be some knee jerk reaction of “No, we can’t do this.” “No, we want this all to be done.” These are all understandable reactions. However, these are reactions, and as with any decision, a response, and not a reaction is most appropriate. A response comes from a place of values, principles, beliefs – ultimately, a place of heart, soul and mind. This is not to suggest that everyone will have the same value response, but everyone can be helped to make a value response, in essence, their best decision. This will be a decision that is consistent with the wishes of their loved one, if known, and if not, consistent with the way their loved one lived his or her life. The best decision for a family is one that the family will feel best about a week from now, a month from now, a year from now.
			</p>
			<p>
				This family centered and value-focused conversation involves truly listening to a family speak of their loved one. This should not be a monologue of a requestor reciting lots of information about donation and the process. The moment of decision is ultimately a moment where the values and beliefs of that one person (the donor) and that of his or her family may meet the needs of people whose lives will be made better by this decision for donation.
			</p>
			<p>
				Simply said, the Designated Tissue Requestor’s role is to sit with a family and sensitively and appropriately discuss how their loved one can enhance and improve the lives of others.
			</p>
			<p>
				Points to consider:
			</p>
			<ul>
				<li>
					Discussing donation provides the family with an opportunity to honor the wishes of the deceased, if those wishes are known.
				</li>
				<li>
					If the deceased’s wishes are not known, the family can reflect on the person’s general feelings, beliefs and values. This will help them to make a decision that is right for their circumstances. Reflection and discussion is an important part of the bereavement process, regardless of the family’s final decision about donation.
				</li>
				<li>
					It empowers the family at a time when they have no control of the circumstances or other decisions being made.
				</li>
			</ul>
			<p>
				When viewed in this light, the donation discussion is not an imposition, but a <strong>continuum of the care</strong> that is offered to the grieving family.
			</p>
			<h3>
				<strong>Donation Discussion Process</strong>
			</h3>
			<p>
				There are six components to the family donation conversation. They are:
			</p>
			<ol class="discussion-process">
				<li>
					Preparing for the discussion
					<ol>
						<li>
							Preparation of self
						</li>
						<li>
							Understanding the family
						</li>
						<li>
							Environment for the family conversation
						</li>
						<li>
							Timing of discussion
						</li>
					</ol>
				</li>
				<li>
					Meeting the family's needs
					<ol>
						<li>
							Developing rapport
						</li>
						<li>
							Effects of bereavement on families
						</li>
					</ol>
				</li>
				<li>
					Introducing donation
				</li>
				<li>
					Providing information
				</li>
				<li>
					Addressing concerns and fears
				</li>
				<li>
					Concluding the conversation
				</li>
			</ol>
			
			<h2>I. Preparing for the discussion</h2>
			<h3>A. Designated Tissue Requestor self-preparation:</h3>
			<p>Before discussing the option of donation with the family/legal decision-maker, know:</p>
			<ul>
				<li>
					The donation options as identified by DNA. If the family asks about donation before you know the options, assure them you will find out – never make assumptions of donor potential.
				</li>
				<li>
					The legally authorized decision maker for the family.
				</li>
				<li>
					Your own feelings and how they might affect the conversation. Have an awareness of feelings you may be experiencing and draw upon your coping skills. It may be helpful to discuss with the family’s spiritual leader or hospital Chaplain, the goal of the family conversation and have them accompany you. If you feel unable to conduct the discussion, for any reason, please contact DNA.
				</li>
				<li>
					Information about the deceased and family obtained through interviewing the physician and the primary care nurse(s) and/or reviewing the medical record, before starting the family conversation.
				</li>
				<li>
					The tools you will need for the interactions with the family include the availability of kleenex and nourishment (coffee, tea, juice), and the authorization form (Arizona Anatomical Gift Authorization)
				</li>
			</ul>
			<h3>B. Understanding the Family:</h3>
			<p>
				Before beginning the conversation* with families about the option of tissue or eye donation, it is important to have some knowledge of their:
			</p>
			<ul>
				<li>
					<strong>Culture</strong> - May be a blend of cultures/ethnicities with customs and beliefs viewed in various levels from strict traditional to more modern. To learn more, see <a href="#" target="_blank">Cultural Indicators</a>
				</li>
				<li>
					<strong>Language</strong> - that which is spoken, understood verbally and/or written.
				</li>
				<li>
					<strong>Communication needs</strong> - such as hearing loss, loss/difficult vision and translation
assistance.
				</li>
			</ul>
			<p>
				* Ask the family members to share with you their beliefs about the end of life. See <a href="#" target="_blank">Religious Beliefs</a>
			</p>
			
			<h3>C. Environment of the Family Conversation:</h3>
			<p>For privacy and security reasons assess the area where family introductions and conversations will take place.</p>
			<ul>
				<li>
					<strong>Seating arrangements</strong> - Should be adequate for you and members of the family.
				</li>
				<li>
					<strong>Location</strong> - Where interactions take place will impact the effectiveness of the discussion. Introductions at the bedside are acceptable, however the donation discussion is most often more successful if done in a private room nearby.
				</li>
				<li>
					<strong>Comfort</strong> - The physical environment and the families themselves can contribute to or distract from the donation discussion. Examples include:
					<ul>
						<li>
							A room that is uncomfortably cool can affect families’ ability to concentrate and may mask body language clues.
						</li>
						<li>
							Noise, outside or within the room can distract and interfere with communication efforts.
						</li>
						<li>
							Interruptions by phones, pagers, and/or hospital staff may come at the wrong moment, hampering the conversation goals.
						</li>
					</ul>
				</li>
			</ul>
			
			<h3>D. Timing the Discussion:</h3>
			<p>DNA must be called <strong>BEFORE</strong> discussing donation with the family.</p>
			<ul>
				<li>
					Avoid offering donation in the same conversation as the notification of death. This allows a
time period between the notification of death and the donation discussion. Providing this break
gives the family time to process information, and increases the likelihood they will donate.
				</li>
				<li>
					Allow the family time to be with the deceased.
				</li>
				<li>
					Don’t rush the family. If necessary, let them know DNA will call them later.
				</li>
			</ul>
			
			<h2>II. Meeting the Family’s Needs</h2>
			<h3>A. Developing Rapport*</h3>
			<p>
				Keep in mind the “Golden Rule” and think about what kind of care you would like to receive if you were in the family’s situation. In the same light, know that what is important to you, may not be for them. Be sensitive to cultural differences.
			</p>
			<ul>
				<li>
					Be mindful that you are an expert caregiver who is also a human being. Your own expression of sadness and even some tears may be a part of your natural response to witnessing the loss of life and the pain of others. Keep in mind that the family may find comfort in knowing that their loved one was not “just another patient” in your hospital.
				</li>
				<li>
					Understand and respect their culture.
				</li>
				<li>
					Know the message you want to deliver. Knowing the information makes it easier to express.
				</li>
				<li>
					Remember for this conversation to speak softly and respectfully.
				</li>
				<li>
					Be sensitive to the language you use. Words can carry powerful meanings.
				</li>
				<li>
					Speak with compassion and acknowledge their pain.
				</li>
				<li>
					Use the patient’s name, the familiar form the family uses.
				</li>
				<li>
					Respect their point of view.
				</li>
				<li>
					Be nonjudgmental.
				</li>
				<li>
					Give simple, concise explanations as often as needed. Use non-medical jargon when possible.
				</li>
				<li>
					Repeat what they say, using their words or phrases.
				</li>
				<li>
					As difficult as it may be to say, gently use the word “dead” in your conversation with the family.
Euphemisms can be confusing and unrealistic; take your lead from the words they use for
death, and use them. Be sensitive to cultures that avoid the word “dead” or “death”.
				</li>
				<li>
					Use open-ended questions that allow families to express themselves freely.
				</li>
				<li>
					Avoid “why” questions; they can make others defensive.
				</li>
				<li>
					Use eye contact when appropriate. Place yourself at the same level as family members. If they
are sitting, you should sit down.
				</li>
				<li>
					Use appropriate nonverbal communication. Exercise judgment when touching someone. Some
families are very comfortable, while others may be offended or angry. Leave arms and legs
uncrossed.
				</li>
				<li>
					Do not be afraid of silence. Sometimes the family may need some time to cry. Quietly being
there can be very supportive. If you think the family may need time alone, ask them. They will
let you know.
				</li>
				<li>
					Have your attention on the other person, not on what you are going to say next.
				</li>
				<li>
					Listen patiently. Let them talk about the deceased or tell the story of the event – it may need to
be repeated many times.
				</li>
				<li>
					Provide a supportive presence. Help with practical matters.
				</li>
				<li>
					Have time for the family. The family may have little sense of time. If you say you’ll be back in
five minutes, it is important to do so.
				</li>
			</ul>
			
			<p class="small-text">
				* adapted from the University of Wisconsin Hospital and Clinics Organ Procurement Organization
			</p>
			<p>
				When you use these tips, you should be very effective in communicating and developing trust with families. Caring for the grieving family is a challenge. Through a clear understanding of eye and tissue donation, sensitivity that crosses cultural barriers, and assessment and communication skills, you will be able to plan and implement care that will assist the family in their grief.
			</p>
			<h3>B. The Effects of Bereavement on the Family Discussion</h3>
			<p>
				Remember at this time, families are thinking of life and death, not donation. They are trying to fill the void, process the death and move on. You can help them through this difficult time.
			</p>
			<p><u>The Four Tasks to Mourning:</u></p>
			<ul>
				<li>
					Accept the reality of the loss
				</li>
				<li>
					Experience the pain of grief
				</li>
				<li>
					Adjust to an environment in which the deceased is missing
				</li>
				<li>
					Emotionally relocate the deceased and move on with life
				</li>
			</ul>
			<p><u><a href="#" target="_blank">Stages of Bereavement</a></u><br>
				The stages a family goes through when learning of the death of a loved one:
			</p>
			<ul>
				<li>
					Shock and numbness
				</li>
				<li>
					Searching and yearning
				</li>
				<li>
					Disorientation
				</li>
				<li>
					Reorganization
				</li>
			</ul>
			
			<h2>III. Introducing Donation</h2>
			<p>
				Finding the exact words to say can be difficult. Often it is not what we say, but how we say it. Using a gentle, unhurried tone will help convey your sympathy. Keep in mind that you are providing the family with information and an opportunity for discussion.
			</p>
			<ul>
				<li>
					<strong>Plan the discussion process</strong>. In order to decrease interruptions, alert the Unit staff that a family discussion will take place, arrange for coverage of your own work responsibilities/tasks for the anticipated discussion. <em>There is not a standard time frame for the discussion – it will vary based upon a number of factors.</em>
				</li>
				<li>
					<strong>Have the hospital staff introduce you.</strong> Wear your name badge. Use formal titles (i.e., “Mrs. Jones”) at the introduction. Ask permission to use preferred name. Using “You” to refer to someone else can be rude and demeaning. (See <a href="#" target="_blank">Cultural Indicators</a> guideline) Ask permission to refer to the deceased using the familiar name. (e.g. May I call him Johnny?)	
				</li>
				<li>
					<strong>Determine the <a href="#" target="_blank">legal decision-maker</a>.</strong> It is not unusual for families to have divided opinions on who is considered the family decision-maker. Interview carefully to determine who is the highest legal authority is for the deceased. <em>The family may introduce the family spokesperson, which in reality may not be the legal authority to make donation decisions</em>
				</li>
				<li>
					<strong>Meet immediate needs.</strong> This may include providing nourishment, providing for a spiritual leader, translator, social worker, list of funeral homes, the availability of a phone and assistance in arranging for travel of distant family, etc).
				</li>
				<li>
					<strong>Actively listen.</strong> Ask the family/legal decision-maker to tell you what they know of the events. <em>Do they understand that death has occurred?</em>
				</li>
				<li>
					<strong>Identify cultural/religious beliefs</strong> related to death and burial. (See <a href="#" target="_blank">Cultural Indicators</a> and <a href="#" target="_blank">Religious Beliefs</a>)
				</li>
				<li>
					<strong>Observe</strong> discretely the family actions and interactions; listening for clues to identify the stage of bereavement they are experiencing. (See the <a href="#" target="_blank">Stages of Bereavement</a> guide to identify stages of bereavement and corresponding suggested actions to meet family needs)
				</li>
				<li>
					<strong>Allow free expression</strong> of grief and mourning without judgment. <em>Provide the family privacy and space.</em>
				</li>
				<li>
					<strong>Do not share your personal experiences</strong> with death or the donation process. <em>This death and grief is their own experience and something they must work through in their own way.</em>
				</li>
			</ul>
			<p>
				Remember that the purpose of the discussion is to offer the family information and invite them to discuss their feelings and share stories of their loved one. Find out “who” the patient was, and offer families the opportunity to “rewrite the final chapter” of their life. <strong>Avoid presenting donation as a “yes or no” question.</strong>
			</p>
			<p>
				To be effective in speaking to families about donation, the family/legal decision-maker should have an established foundation for the discussion. Let the legal decision-maker and/or family know that you need to discuss some information in private (example: name of funeral home, special wishes for the care of the body, disposition of belongings, etc.) Ask if there is anyone else who needs to be involved in such a discussion.
			</p>
			<p>
				In order to provide adequate information, the discussion needs to include the following key points:
			</p>
			<ul>
				<li>
					Their loved one will be treated with respect and dignity.
				</li>
				<li>
					How their gift makes a difference.
				</li>
				<li>
					Donation does not generally interfere with the timing of the funeral and will not prevent an
open-casket service. If they have time constraints, address this with the family.
				</li>
				<li>
					There is no cost to the family for donation.
				</li>
			</ul>
			<h2>Providing Information</h2>
			<p>
				As a Designated Tissue Requestor, you are an advocate for proper discussions and accurate information for the families. It is important that your knowledge stay current. For more information see: <a href="#" target="_blank">Tissue Fact Sheet</a>. Current waiting list numbers can be found on <a href="http://www.unos.org" target="_blank">www.unos.org</a>.
			</p>
			<ul>
				<li>
					More than 100 people in need can benefit from one person’s decision to be an eye and tissue donor.
				</li>
				<li>
					With recent advances in medical technology, many more people than ever before can be donors, even those in their 70s and 80s.
				</li>
				<li>
					Approximately 1.5 million tissue transplants occur in the U.S. each year.
				</li>
				<li>
					Approximately 44,000 sight-restoring cornea transplants are performed every year.
				</li>
			</ul>
			<p>
				For a family considering donation to make an informed decision, they will need to know about the recovery process:
			</p>
			<ul>
				<li>
					Tissue donation (bone, heart valves, pericardium, skin, connective tissue and veins) requires detailed information about the recovery process.
				</li>
				<li>
					Transplantation is the leading medical treatment for corneal blindness.
				</li>
				<li>
					Inform the family that the primary purpose for eye donation is to restore sight through corneal
transplant.
				</li>
				<li>
					If it is determined that the corneas cannot be transplanted due to medical reasons, the tissue
may be used for education and study into the treatment of eye disease, provided the family
has given permission for research.
				</li>
				<li>
					Skin that is recovered is very thin and will leave a reddened area, similar to a sunburn, on the
back, abdomen, or legs.
				</li>
				<li>
					With heart valves, the whole heart is carefully recovered through an incision in the chest.
Though the surgical marks will remain, they will be in areas normally covered by clothing.
				</li>
				<li>
					Donated bone is carefully removed, and a form is put in place to maintain the natural shape of
the body.
				</li>
				<li>
					As in any surgical procedure, the area is carefully closed.
				</li>
				<li>
					For additional information see the <a href="#" target="_blank">Tissue Fact Sheet</a>.
				</li>
			</ul>
			<p>
				The days of a Designated Tissue Requestor approaching a family and simply asking the question “Do you want your loved one to be a tissue donor?” are hopefully gone. In fact, that question only invites a knee jerk response of “no”. It is important to keep the focus on the patient’s and family’s values and beliefs. Part of families making their best decision is for them to have all the information about donation presented.
			</p>
			
			<h3><u>DO's</u></h3>
			<p>
				Once they have processed the death, let them know: how many lives can be impacted; which tissues can be donated; who will be involved in the donation process, that the recovery of tissues takes place respectfully; the time frames involved in the donation process; how recipients of tissue donation go on to lead better lives. This is an informed conversation that will result in an informed decision. Anything short of sharing this kind of in-depth information and conversation deprives the family of what they need to offer their best response.
			</p>
			<h3><u>DON'Ts</u></h3>
			<p>
				Consider also, the role language plays in the misconceptions and fears about organ and tissue donation. It is important for us to remember to keep the feelings of donor families in mind when we write or talk about donation. As of May 2004, the following terminology was approved by the Association of Organ Procurement Organizations (AOPO) Donor Family Council.
			</p>
			<p>Please use:<br>
				<strong>“Recover”</strong> tissue instead of <strong>“harvest”</strong>
				<br>
				<br>
				<strong>“Surgical Recovery”</strong> of tissue instead of <strong>“harvesting”</strong> of tissue.<br>
Most people associate the word <strong>“harvest”</strong> with crops, crows, and combines. This word leaves a negative impression when connected with donation. The word <strong>“recovery”</strong> helps people understand that the removal of a loved one’s tissue is a respectable surgical procedure.
			</p>
			<p>
				<strong>“Deceased Donor”</strong> instead of <strong>“cadaver”</strong>
				<br><br>
				<strong>“Deceased” Donation</strong> instead of <strong>“cadaveric”</strong><br>
Today, as more people choose to become living donors, there is a need to distinguish between living and <strong>deceased</strong> donors. The term <strong>cadaveric</strong> depersonalizes the fact that a gift was offered to someone upon an individual’s death. Webster defines cadaver as “dead bodies intended for dissection.” It can be very difficult for donor families to hear their loved one spoken of in this regard.
			</p>
			<p>
				When the family has made a decision, support their decision. For example: “I honor your decision. I know it is the right decision for you and your family. Thank you for your time, and considering donation. I am sorry for your loss.”
			</p>
			<p>
				Remember that you are not only a Designated Tissue Requestor for tissue and eye donation, you are also representative of all donation. As a healthcare professional, the community will look to you for accurate information on donation. Additional information about organ donation can be found <a href="#" target="_blank">here.</a>
			</p>
			
			<h2>V. Addressing Concerns and Fears</h2>
			<p>
				Many myths and misinformation surround the topic of donation. It is important that you address these issues with the family to help alleviate any concerns or fears they may have.
			</p>
			<p>
				<strong>Fact:</strong> Donation does not disfigure the body or change the way a person looks. Donated organs and tissues are removed surgically, as in a routine operation. Tissue donation optimally occurs within 12 hours of death and generally wouldn’t delay funeral arrangements.
			</p>
			<p>
				<strong>Fact:</strong> There is no cost to your estate or family for donation.
			</p>
			<p>
				<strong>Fact:</strong> No major religions oppose donation.
			</p>
			<p>
				<strong>Fact:</strong> People in America receive approximately 1.5 million tissue transplants annually. Transplantation is a standard medical procedure, and success rates are extremely high.
			</p>
			<p>
				<strong>Fact:</strong> Studies show that donation most often provides immediate and long-term consolation. Donation can be especially comforting when the death is unexpected and the donor is young.
			</p>
			<p>
				<strong>Fact:</strong> People of different age groups and medical histories can potentially be donors.
			</p>
			
			<h2>VI. Concluding the Conversation</h2>
			<p>
				If the family is anxious to leave, please obtain phone numbers where they may be reached in the next few hours.
			</p>
			<p>
				Discussion closure
				<ul>
					<li>
						If authorization is granted let the family know that DNA staff will be contacting them to review the potential donor’s medical/social history with them.
					</li>
					<li>
						Give them a copy of the signed Authorization Form.
					</li>
					<li>
						Should they have questions, point out DNA’s phone number, 1-800-447-9477, on the bottom of
the Authorization Form.
					</li>
					<li>
						Let them know it is okay to go home and call the hospital’s social/family services for assistance
as needed.
					</li>
				</ul>
			</p>
			
			</article>
		</div>
	</section>
        <section>
    <div class="row">
                <form role="form" method="POST" action="http://www.donornetwork.dev/certification/section/5">
            <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
            <div class="row">
                <div class="small-12 columns">
                    <h2>Section 5 Quiz: Answer the questions correctly to go to the next section</h2>
                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>16. In determining when to begin the family conversation about donation which factors should be considered:</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ufrpubaedpsoerrfpbtunvquwfnoboeq]" value="dadddrnqcnrpervcuovvnvnusvtpwfff"
         />
    </div>
    <div class="small-11 columns">
        a. length of time the family has known about the death
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ufrpubaedpsoerrfpbtunvquwfnoboeq]" value="qvbaranpapodrpewqufrcofcouttupnb"
         />
    </div>
    <div class="small-11 columns">
        b. patient&#039;s donation potential
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ufrpubaedpsoerrfpbtunvquwfnoboeq]" value="wqcneeonfnfdwpnoudcvaeafoqtfaanb"
         />
    </div>
    <div class="small-11 columns">
        c. talking with the family in a quiet, private, place
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ufrpubaedpsoerrfpbtunvquwfnoboeq]" value="pvocetbesdeertvwrsqcttntfvrqocur"
         />
    </div>
    <div class="small-11 columns">
        d. A and B only
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ufrpubaedpsoerrfpbtunvquwfnoboeq]" value="oweswcvbqucbfucvodrvuvwbwueaeraq"
         />
    </div>
    <div class="small-11 columns">
        e. All the above are true
    </div>
</div>
                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>17. Offering the option of eye and tissue donation to a grieving family can empower  the family during this difficult time.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[foaeafwcswfroovosdttubuttubrnbef]" value="fvqqfbpctwqbqupurbbnqtbcnsbsnqtf"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[foaeafwcswfroovosdttubuttubrnbef]" value="reowfwqaredueefwtrauosrdccqnsuve"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>18. It is important to ask a family about donation as soon as possible after notifying them of their loved one&#039;s death in order to minimize the amount of emotional distress.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[onfadseapnrteavbuconauwbwrwpdpuq]" value="buqesstqboundaubbsqcpfnufcaqqffe"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[onfadseapnrteavbuconauwbwrwpdpuq]" value="eutcfvodbqdbqqsppwropsbsaabafpes"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>19. The family does not pay any cost associated with donation.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[foaaafupfqpesdaancqqwqecuntbwfst]" value="dwconqecodtfptcfebapwussnuvfrneq"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[foaaafupfqpesdaancqqwqecuntbwfst]" value="verbcfvfqfdnqcfpnuwsbswwrcrwvfwo"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>20. Families should feel supported, no matter what decision was made.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[wbcdtqownqoqdaancfuusvtfbsqecoeu]" value="dwtoeqbacfdbfrvctwdbndbtbnsbparn"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[wbcdtqownqoqdaancfuusvtfbsqecoeu]" value="wtacqnndtqrovcqscfbvftnebqwccdft"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>21. Let families know that cadaveric donation is the means by which tissue, eye and bone can be recovered.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ebenbwvarcuvcbtfqbvrcetttqftobbn]" value="nbtetntrnttuacrbstevpatnsqfbndeu"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[ebenbwvarcuvcbtfqbvrcetttqftobbn]" value="oqpvtoavfnsnnaecuebsqvqrcqaornwr"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                        <div class="row">
                <div class="small-12 columns">
                    <div>22. Directly asking a family if they want their loved one to be a tissue donor is clear, and therefore the preferred method of discussing donation with a family.</div>
                    <div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[qbtbovntnobauwfqttduunrerqvsscve]" value="ecnuenbuvdstbuusqfpnrwdqbnuudwcw"
         />
    </div>
    <div class="small-11 columns">
        TRUE
    </div>
</div>
<div class="row">
    <div class="small-1 columns text-right">
        <input type="radio" name="responses[qbtbovntnobauwfqttduunrerqvsscve]" value="peorocodswsrebeptrqdrsudrrqcbnwd"
         />
    </div>
    <div class="small-11 columns">
        FALSE
    </div>
</div>                </div>
            </div>
                         <div class="row">
                <div class="small-12 columns">
                    <a href="#" class="button small radius submit">Continue &gt; &gt;</a>
                </div>
            </div>
        </form>
    </div>
</section></body>
</html>