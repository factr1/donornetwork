
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>

</head>
<body>
	<section>
		<div class="row">
			<div class="medium-12 columns">
				<article>
					<h2>Objective Two: Identify key legislation and hospital compliance requirements regarding the donation process, including the Donor Registry.</h2>
					<h3>A: Current Legislation</h3>
					<p>
						The most current version of the Arizona Revised Uniform Anatomical Gift Act (2007) withdrew the need for signature when declining donation, modified the hierarchy of authorization (see <a href="#" target="_blank">Authorization Form English</a> or <a href="#" target="_blank">Authorization Form Spanish</a> for details), and lowered the minimum age for a person to register to donate.</p>
					<p>
Arizona Revised Uniform Anatomical Gift Act :
<a href="http://www.azleg.gov/FormatDocument.asp?inDoc=/ars/36/00841.htm&Title=36&DocType=ARS" target="_blank">Click here</a>.
					</p>
					<h3>B: Hospital Compliance</h3>
					<p>
						In 1998, the Centers for Medicare and Medicaid Services (CMS) required hospitals make timely referrals of all patient deaths to their OPO (Donor Network of Arizona). In 2007, CMS defined a timely referral to be within one hour of imminent death or cardiac death.
					</p>
					<p>
					For more information: CMS Conditions of Participation:
					<a href="http://a257.g.akamaitech.net/7/257/2422/12feb20041500/edocket.access.gpo.gov/cfr_2004/octqtr/pdf /42cfr482.45.pdf" target="_blank">Click here</a>.
					</p>
					<h3>C: Donor Designation and your Driver's License</h3>
					<p>
						Currently, when you apply for an Arizona license, you will be given the opportunity to be a registered donor. By checking the donor box, you will automatically be entered into the Arizona Donor Registry. As of June 2014, a red donor heart will be printed on your license.
					</p>
					<h3>D: The Donor Registry</h3>
					<p>
						The Arizona Donor Registry was established in 2003 as the official record for donation. Registering as a donor is considered a first person authorization for donation. While a person as young as 15 years, 6 months, may register as a donor, until the age of 18, parental/guardian consent is required at the time of donation. Registering makes the decision to donate legal and binding and protects a person’s intent to donate. The family cannot override this authorization. For more information: <a href="www.DonateLifeAZ.org" target="_blank">www.DonateLifeAZ.org</a>
					</p>
				</article>
			</div>
		</div>
		<div class="row">
			<form role="form" method="POST" action="http://www.donornetwork.dev/certification/section/2">
				<input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
				<div class="row">
					<div class="small-12 columns">
						<h2>Section 2 Quiz: Answer the questions correctly to go to the next section</h2>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<div>7. According to federal regulations, Donor Network of Arizona must be contacted,    within one hour of cardiac death, each time a patient dies within your hospital.</div>
						<div class="row">
							<div class="small-1 columns text-right">
								<input type="radio" name="responses[ffutwqaabtsosavdvuvnavptvvdcrdos]" value="vupvsvcereqrcftvdprvrswwutapbupf"/>
							</div>
							<div class="small-11 columns">
								TRUE
							</div>
						</div>
						<div class="row">
							<div class="small-1 columns text-right">
								<input type="radio" name="responses[ffutwqaabtsosavdvuvnavptvvdcrdos]" value="oavwdsatwvwwnofbuspptbnnecdnasbb"/>
							</div>
						<div class="small-11 columns">
							FALSE
						</div>
					</div>                
				</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<div>8. A family can override a person&#039;s (excluding minors) decision to donate as documented in the donor registry.</div>
				<div class="row">
					<div class="small-1 columns text-right">
						<input type="radio" name="responses[snefawenbpcdbdpscqnvsauqsbfqrtcr]" value="vqcawpsrnvuobtqcuusfqdbaawrvousb"/>
					</div>
					<div class="small-11 columns">
						TRUE
					</div>
				</div>
				<div class="row">
					<div class="small-1 columns text-right">
						<input type="radio" name="responses[snefawenbpcdbdpscqnvsauqsbfqrtcr]" value="cdwrobfaapsundetpbqwrcansbodfpur"/>
					</div>
					<div class="small-11 columns">
						FALSE
					</div>
				</div>                
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<a href="#" class="button small radius submit">Continue &gt; &gt;</a>
			</div>
		</div>
		</form>
		</div>
	</section>
</body>
</html>