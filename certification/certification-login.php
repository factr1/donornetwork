
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow" />
    <title></title>
    <meta name="author" content="Factor1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!--
    <meta name="msapplication-config" content=" /browserconfig.xml"/>
    -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300" />
    <link type="text/css" rel="stylesheet" href="css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="css/cert.css">
    <script src="js/app.min.js" type="text/javascript"></script>
</head>
<body>
	<section class="row">
		<div class="small-12 columns">
			<h2>Online Recertification for Designated Tissue Requestor for Eye & Tissue Donation</h2>
			<p>Thank you for taking the time to renew your certification as a Certified Designated Tissue Requestor for eye and tissue donation. It is only through the efforts of professionals such as yourself that countless others receive the gift of eye or tissue transplantation. All information is for internal use only and will be not be shared with outside parties.</p>
			
			<p>
			After completing the log on, please proceed through the course. Answer the questions as they appear. If your answer is incorrect, the test will prompt you to repeat the questions in that section. After completion of the course and course evaluation, you can print a certificate of completion. If you would like the certificate mailed to you, please send your name and address to <a href="mailto:Melissa.Gallagher@dnaz.org">Melissa.Gallagher@dnaz.org</a>.
		</p>
		<p>
			We appreciate your evaluation of this course, and if you have any questions or concerns please contact <a href="mailto:Lisa.Kula@dnaz.org">Lisa.Kula@dnaz.org</a>.
		</p>
		</div>
	</section>
    <section>
        <div class="row">
        <div class="small-6 columns">
            <form role="form" method="POST" action="http://www.donornetwork.dev/register">
    <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
    <fieldset>
        <legend>Register to Begin</legend>
        <div class="row">
            <div class="small-12 columns">
                <label>First Name
                    <input type="text" name="first_name" value="" placeholder="name" required />
                </label>
                <label>Middle Initial
                    <input type="text" name="middle_name" value="" placeholder="initial" />
                </label>
                <label>Last Name
                    <input type="text" name="last_name" value="" placeholder="name" required />
                </label>
                <label>Email
                    <input type="email" name="email" value="" placeholder="email" required />
                </label>
                <label>Password
                    <input type="password" name="password" value="" placeholder="8-255 characters" required />
                </label>
                <label>Confirm Password
                    <input type="password" name="password_confirmation" value="" placeholder="password" required />
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label>Hospital
                    <select name="hospital_id">
                        <option value="">Select Hospital...</option>
                                                <option value="1">Employee</option>
                                            </select>
                </label>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="small-12 columns">
                <label>Unit
                    <input type="text" name="unit" value="" placeholder="hospital unit" />
                </label>
                <label>RN Number
                    <input type="text" name="rn_number" value="" placeholder="RN number" />
                </label>
                <label>
                    <input type="checkbox" name="optin_donation_updates" value="1" class="inline" /> I would be interested in receiving informational updates on donation.
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <a href="#" class="button small radius submit">Register</a>
            </div>
        </div>
    </feildset>
</form>        </div>
        <div class="small-6 columns">
            <form role="form" method="POST" action="http://www.donornetwork.dev/login">
    <input type="hidden" name="_token" value="eJzK3vWw29so7n9ZInszYdfxOP6POuYVStiDIg36" />
    <fieldset>
        <legend>Already Registered? Login to begin test.</legend>
        <div class="row">
            <div class="small-12 columns">
                <label>
                    <input type="email" name="email" value="" placeholder="Email Address" required />
                </label>
                <label>
                    <input type="password" name="password" value="" placeholder="Password" required />
                </label>
                <input type="checkbox" name="remember" /><label for="remember">Remember Me</label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <a href="#" class="button small radius submit">Login</a>
                <a href="http://www.donornetwork.dev/password/email">Forgot Your Password?</a>
            </div>
        </div>
    </feildset>
</form>
        </div>
    </div>
</section>

<section class="row">
	<div class="small-12 legal-container columns">
		<p class="legal">
			<strong>Approval Statement:</strong><br>
			The Donor Network of Arizona is an approved provider of continuing nursing education by the Western Multi-State Division, an accredited approver by the American Nurses Credentialing Center’s Commission on Accreditation.
		</p>
		<p class="legal">
			<strong>Disclaimer Statements:</strong><br>
			<u>In compliance with the guidelines for approved Contact Hour activities the following statements are offered for your review:</u><br>
			There are no financial conflicts of interest from the presenters for this approved course.
		</p>
		<p class="legal">
			All individuals in positions to control content of the educational activity have disclosed all financial relationships and there are no conflicts of interest.
		</p>
		<p class="legal">
			There is no commercial support of this educational activity.
		</p>
		<p class="legal">
			AzNA and the ANCC Commission on Accreditation do not approve or endorse any commercial products displayed.
		</p>
		<p class="legal">
			There is no off-label usage/no product related to this activity.
		</p>
		
	</div>
</section>

</body>
</html>